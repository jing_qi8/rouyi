package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 个人用户基本信息开户对象 huifutianxia_user_indv
 * 
 * @author ruoyi
 * @date 2023-10-14
 */
public class HuifutianxiaUserIndv extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 汇付天下个人户id */
    private Long id;

    /** 请求流水号 */
    @Excel(name = "请求流水号")
    private String reqSeqId;

    /** 商户ID */
    @Excel(name = "商户ID")
    private String merchantId;

    /** 汇付ID */
    @Excel(name = "汇付ID")
    private String huifuId;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 身份证号码 */
    @Excel(name = "身份证号码")
    private String certNo;

    /** 身份证起始日期 */
    @Excel(name = "身份证起始日期")
    private String certBeginDate;

    /** 身份证截止日期 */
    @Excel(name = "身份证截止日期")
    private String certEndDate;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobileNo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setReqSeqId(String reqSeqId) 
    {
        this.reqSeqId = reqSeqId;
    }

    public String getReqSeqId() 
    {
        return reqSeqId;
    }
    public void setMerchantId(String merchantId) 
    {
        this.merchantId = merchantId;
    }

    public String getMerchantId() 
    {
        return merchantId;
    }
    public void setHuifuId(String huifuId) 
    {
        this.huifuId = huifuId;
    }

    public String getHuifuId() 
    {
        return huifuId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCertNo(String certNo) 
    {
        this.certNo = certNo;
    }

    public String getCertNo() 
    {
        return certNo;
    }
    public void setCertBeginDate(String certBeginDate) 
    {
        this.certBeginDate = certBeginDate;
    }

    public String getCertBeginDate() 
    {
        return certBeginDate;
    }
    public void setCertEndDate(String certEndDate) 
    {
        this.certEndDate = certEndDate;
    }

    public String getCertEndDate() 
    {
        return certEndDate;
    }
    public void setMobileNo(String mobileNo) 
    {
        this.mobileNo = mobileNo;
    }

    public String getMobileNo() 
    {
        return mobileNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("reqSeqId", getReqSeqId())
            .append("merchantId", getMerchantId())
            .append("huifuId", getHuifuId())
            .append("name", getName())
            .append("certNo", getCertNo())
            .append("certBeginDate", getCertBeginDate())
            .append("certEndDate", getCertEndDate())
            .append("mobileNo", getMobileNo())
            .append("createTime", getCreateTime())
            .toString();
    }
}
