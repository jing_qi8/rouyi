package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 汇付天下余额支付对象 huifutianxia_yue_pay
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
public class HuifutianxiaYuePay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 余额支付表ID */
    private Long id;

    /** 汇付申请订单号 */
    @Excel(name = "汇付申请订单号")
    private String reqSeqId;

    /** 出款方商户号 */
    @Excel(name = "出款方商户号")
    private String outHuifuId;

    /** 订单总金额 */
    @Excel(name = "订单总金额")
    private String ordAmt;

    /** 分账金额 */
    @Excel(name = "分账金额")
    private String divAmt;

    /** 被分账方ID */
    @Excel(name = "被分账方ID")
    private String huifuId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setReqSeqId(String reqSeqId) 
    {
        this.reqSeqId = reqSeqId;
    }

    public String getReqSeqId() 
    {
        return reqSeqId;
    }
    public void setOutHuifuId(String outHuifuId) 
    {
        this.outHuifuId = outHuifuId;
    }

    public String getOutHuifuId() 
    {
        return outHuifuId;
    }
    public void setOrdAmt(String ordAmt) 
    {
        this.ordAmt = ordAmt;
    }

    public String getOrdAmt() 
    {
        return ordAmt;
    }
    public void setDivAmt(String divAmt) 
    {
        this.divAmt = divAmt;
    }

    public String getDivAmt() 
    {
        return divAmt;
    }
    public void setHuifuId(String huifuId) 
    {
        this.huifuId = huifuId;
    }

    public String getHuifuId() 
    {
        return huifuId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("reqSeqId", getReqSeqId())
            .append("outHuifuId", getOutHuifuId())
            .append("ordAmt", getOrdAmt())
            .append("divAmt", getDivAmt())
            .append("huifuId", getHuifuId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
