package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 支付商户信息对象 pay_merchant
 * 
 * @author ruoyi
 * @date 2023-10-06
 */
public class PayMerchant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商户编号 */
    private Long id;

    /** 商户号 */
    @Excel(name = "商户号")
    private String no;

    /** 商户全称 */
    @Excel(name = "商户全称")
    private String name;

    /** 商户简称 */
    @Excel(name = "商户简称")
    private String shortName;

    /** 调用地址 */
    @Excel(name = "调用地址")
    private String payUrl;

    /** 公钥地址 */
    @Excel(name = "公钥地址")
    private String publicKeyPath;

    /** 私钥地址 */
    @Excel(name = "私钥地址")
    private String privateKeyPath;

    /** 钥匙密码 */
    @Excel(name = "钥匙密码")
    private String keyPassword;

    /** 开启状态 */
    @Excel(name = "开启状态")
    private Long status;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 更新者 */
    @Excel(name = "更新者")
    private String updater;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private Long tenantId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNo(String no) 
    {
        this.no = no;
    }

    public String getNo() 
    {
        return no;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setShortName(String shortName) 
    {
        this.shortName = shortName;
    }

    public String getShortName() 
    {
        return shortName;
    }
    public void setPayUrl(String payUrl) 
    {
        this.payUrl = payUrl;
    }

    public String getPayUrl() 
    {
        return payUrl;
    }
    public void setPublicKeyPath(String publicKeyPath) 
    {
        this.publicKeyPath = publicKeyPath;
    }

    public String getPublicKeyPath() 
    {
        return publicKeyPath;
    }
    public void setPrivateKeyPath(String privateKeyPath) 
    {
        this.privateKeyPath = privateKeyPath;
    }

    public String getPrivateKeyPath() 
    {
        return privateKeyPath;
    }
    public void setKeyPassword(String keyPassword) 
    {
        this.keyPassword = keyPassword;
    }

    public String getKeyPassword() 
    {
        return keyPassword;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setCreator(String creator) 
    {
        this.creator = creator;
    }

    public String getCreator() 
    {
        return creator;
    }
    public void setUpdater(String updater) 
    {
        this.updater = updater;
    }

    public String getUpdater() 
    {
        return updater;
    }
    public void setDeleted(Integer deleted) 
    {
        this.deleted = deleted;
    }

    public Integer getDeleted() 
    {
        return deleted;
    }
    public void setTenantId(Long tenantId) 
    {
        this.tenantId = tenantId;
    }

    public Long getTenantId() 
    {
        return tenantId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("no", getNo())
            .append("name", getName())
            .append("shortName", getShortName())
            .append("payUrl", getPayUrl())
            .append("publicKeyPath", getPublicKeyPath())
            .append("privateKeyPath", getPrivateKeyPath())
            .append("keyPassword", getKeyPassword())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("creator", getCreator())
            .append("createTime", getCreateTime())
            .append("updater", getUpdater())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .append("tenantId", getTenantId())
            .toString();
    }
}
