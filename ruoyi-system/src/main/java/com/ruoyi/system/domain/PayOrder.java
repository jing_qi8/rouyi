package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 支付订单
对象 pay_order
 * 
 * @author ruoyi
 * @date 2023-09-30
 */
public class PayOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 支付订单编号 */
    private Long id;

    /** 商户编号 */
    @Excel(name = "商户编号")
    private Long merchantId;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private Long appId;

    /** 渠道编号 */
    @Excel(name = "渠道编号")
    private Long channelId;

    /** 渠道编码 */
    @Excel(name = "渠道编码")
    private String channelCode;

    /** 商户订单编号 */
    @Excel(name = "商户订单编号")
    private String merchantOrderId;

    /** 商品标题 */
    @Excel(name = "商品标题")
    private String subject;

    /** 商品描述 */
    @Excel(name = "商品描述")
    private String body;

    /** 异步通知地址 */
    @Excel(name = "异步通知地址")
    private String notifyUrl;

    /** 通知商户支付结果的回调状态 */
    @Excel(name = "通知商户支付结果的回调状态")
    private Long notifyStatus;

    /** 支付金额，单位：分 */
    @Excel(name = "支付金额，单位：分")
    private Long amount;

    /** 渠道手续费，单位：百分比 */
    @Excel(name = "渠道手续费，单位：百分比")
    private Long channelFeeRate;

    /** 渠道手续金额，单位：分 */
    @Excel(name = "渠道手续金额，单位：分")
    private Long channelFeeAmount;

    /** 支付状态 */
    @Excel(name = "支付状态")
    private Long status;

    /** 用户 IP */
    @Excel(name = "用户 IP")
    private String userIp;

    /** 订单失效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单失效时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expireTime;

    /** 订单支付成功时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单支付成功时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date successTime;

    /** 订单支付通知时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单支付通知时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date notifyTime;

    /** 支付成功的订单拓展单编号 */
    @Excel(name = "支付成功的订单拓展单编号")
    private Long successExtensionId;

    /** 退款状态 */
    @Excel(name = "退款状态")
    private Long refundStatus;

    /** 退款次数 */
    @Excel(name = "退款次数")
    private Long refundTimes;

    /** 退款总金额，单位：分 */
    @Excel(name = "退款总金额，单位：分")
    private Long refundAmount;

    /** 渠道用户编号 */
    @Excel(name = "渠道用户编号")
    private String channelUserId;

    /** 渠道订单号 */
    @Excel(name = "渠道订单号")
    private String channelOrderNo;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creator;

    /** 更新者 */
    @Excel(name = "更新者")
    private String updater;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer deleted;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private Long tenantId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMerchantId(Long merchantId) 
    {
        this.merchantId = merchantId;
    }

    public Long getMerchantId() 
    {
        return merchantId;
    }
    public void setAppId(Long appId) 
    {
        this.appId = appId;
    }

    public Long getAppId() 
    {
        return appId;
    }
    public void setChannelId(Long channelId) 
    {
        this.channelId = channelId;
    }

    public Long getChannelId() 
    {
        return channelId;
    }
    public void setChannelCode(String channelCode) 
    {
        this.channelCode = channelCode;
    }

    public String getChannelCode() 
    {
        return channelCode;
    }
    public void setMerchantOrderId(String merchantOrderId) 
    {
        this.merchantOrderId = merchantOrderId;
    }

    public String getMerchantOrderId() 
    {
        return merchantOrderId;
    }
    public void setSubject(String subject) 
    {
        this.subject = subject;
    }

    public String getSubject() 
    {
        return subject;
    }
    public void setBody(String body) 
    {
        this.body = body;
    }

    public String getBody() 
    {
        return body;
    }
    public void setNotifyUrl(String notifyUrl) 
    {
        this.notifyUrl = notifyUrl;
    }

    public String getNotifyUrl() 
    {
        return notifyUrl;
    }
    public void setNotifyStatus(Long notifyStatus) 
    {
        this.notifyStatus = notifyStatus;
    }

    public Long getNotifyStatus() 
    {
        return notifyStatus;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setChannelFeeRate(Long channelFeeRate) 
    {
        this.channelFeeRate = channelFeeRate;
    }

    public Long getChannelFeeRate() 
    {
        return channelFeeRate;
    }
    public void setChannelFeeAmount(Long channelFeeAmount) 
    {
        this.channelFeeAmount = channelFeeAmount;
    }

    public Long getChannelFeeAmount() 
    {
        return channelFeeAmount;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setUserIp(String userIp) 
    {
        this.userIp = userIp;
    }

    public String getUserIp() 
    {
        return userIp;
    }
    public void setExpireTime(Date expireTime) 
    {
        this.expireTime = expireTime;
    }

    public Date getExpireTime() 
    {
        return expireTime;
    }
    public void setSuccessTime(Date successTime) 
    {
        this.successTime = successTime;
    }

    public Date getSuccessTime() 
    {
        return successTime;
    }
    public void setNotifyTime(Date notifyTime) 
    {
        this.notifyTime = notifyTime;
    }

    public Date getNotifyTime() 
    {
        return notifyTime;
    }
    public void setSuccessExtensionId(Long successExtensionId) 
    {
        this.successExtensionId = successExtensionId;
    }

    public Long getSuccessExtensionId() 
    {
        return successExtensionId;
    }
    public void setRefundStatus(Long refundStatus) 
    {
        this.refundStatus = refundStatus;
    }

    public Long getRefundStatus() 
    {
        return refundStatus;
    }
    public void setRefundTimes(Long refundTimes) 
    {
        this.refundTimes = refundTimes;
    }

    public Long getRefundTimes() 
    {
        return refundTimes;
    }
    public void setRefundAmount(Long refundAmount) 
    {
        this.refundAmount = refundAmount;
    }

    public Long getRefundAmount() 
    {
        return refundAmount;
    }
    public void setChannelUserId(String channelUserId) 
    {
        this.channelUserId = channelUserId;
    }

    public String getChannelUserId() 
    {
        return channelUserId;
    }
    public void setChannelOrderNo(String channelOrderNo) 
    {
        this.channelOrderNo = channelOrderNo;
    }

    public String getChannelOrderNo() 
    {
        return channelOrderNo;
    }
    public void setCreator(String creator) 
    {
        this.creator = creator;
    }

    public String getCreator() 
    {
        return creator;
    }
    public void setUpdater(String updater) 
    {
        this.updater = updater;
    }

    public String getUpdater() 
    {
        return updater;
    }
    public void setDeleted(Integer deleted) 
    {
        this.deleted = deleted;
    }

    public Integer getDeleted() 
    {
        return deleted;
    }
    public void setTenantId(Long tenantId) 
    {
        this.tenantId = tenantId;
    }

    public Long getTenantId() 
    {
        return tenantId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("merchantId", getMerchantId())
            .append("appId", getAppId())
            .append("channelId", getChannelId())
            .append("channelCode", getChannelCode())
            .append("merchantOrderId", getMerchantOrderId())
            .append("subject", getSubject())
            .append("body", getBody())
            .append("notifyUrl", getNotifyUrl())
            .append("notifyStatus", getNotifyStatus())
            .append("amount", getAmount())
            .append("channelFeeRate", getChannelFeeRate())
            .append("channelFeeAmount", getChannelFeeAmount())
            .append("status", getStatus())
            .append("userIp", getUserIp())
            .append("expireTime", getExpireTime())
            .append("successTime", getSuccessTime())
            .append("notifyTime", getNotifyTime())
            .append("successExtensionId", getSuccessExtensionId())
            .append("refundStatus", getRefundStatus())
            .append("refundTimes", getRefundTimes())
            .append("refundAmount", getRefundAmount())
            .append("channelUserId", getChannelUserId())
            .append("channelOrderNo", getChannelOrderNo())
            .append("creator", getCreator())
            .append("createTime", getCreateTime())
            .append("updater", getUpdater())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .append("tenantId", getTenantId())
            .toString();
    }
}
