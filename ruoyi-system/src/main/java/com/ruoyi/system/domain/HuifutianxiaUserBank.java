package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 汇付天下用户业务入驻对象 huifutianxia_user_bank
 * 
 * @author ruoyi
 * @date 2023-10-14
 */
public class HuifutianxiaUserBank extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 汇付天下个人银行卡id */
    private Long id;

    /** 请求流水号 */
    @Excel(name = "请求流水号")
    private String reqSeqId;

    /** 开卡姓名 */
    @Excel(name = "开卡姓名")
    private String cardName;

    /** 银行卡卡号 */
    @Excel(name = "银行卡卡号")
    private String cardNo;

    /** 省ID */
    @Excel(name = "省ID")
    private String provId;

    /** 市ID */
    @Excel(name = "市ID")
    private String areaId;

    /** 汇付取现号 */
    @Excel(name = "汇付取现号")
    private String tokenNo;

    /** 分账金额 */
    @Excel(name = "分账金额")
    private String divAmt;

    /** 汇付ID */
    @Excel(name = "汇付ID")
    private String huifuId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setReqSeqId(String reqSeqId) 
    {
        this.reqSeqId = reqSeqId;
    }

    public String getReqSeqId() 
    {
        return reqSeqId;
    }
    public void setCardName(String cardName) 
    {
        this.cardName = cardName;
    }

    public String getCardName() 
    {
        return cardName;
    }
    public void setCardNo(String cardNo) 
    {
        this.cardNo = cardNo;
    }

    public String getCardNo() 
    {
        return cardNo;
    }
    public void setProvId(String provId) 
    {
        this.provId = provId;
    }

    public String getProvId() 
    {
        return provId;
    }
    public void setAreaId(String areaId) 
    {
        this.areaId = areaId;
    }

    public String getAreaId() 
    {
        return areaId;
    }
    public void setTokenNo(String tokenNo) 
    {
        this.tokenNo = tokenNo;
    }

    public String getTokenNo() 
    {
        return tokenNo;
    }
    public void setDivAmt(String divAmt) 
    {
        this.divAmt = divAmt;
    }

    public String getDivAmt() 
    {
        return divAmt;
    }
    public void setHuifuId(String huifuId) 
    {
        this.huifuId = huifuId;
    }

    public String getHuifuId() 
    {
        return huifuId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("reqSeqId", getReqSeqId())
            .append("cardName", getCardName())
            .append("cardNo", getCardNo())
            .append("provId", getProvId())
            .append("areaId", getAreaId())
            .append("tokenNo", getTokenNo())
            .append("divAmt", getDivAmt())
            .append("huifuId", getHuifuId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
