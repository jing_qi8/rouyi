package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 汇付天下提现对象 huifutianxia_enchashment
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
public class HuifutianxiaEnchashment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 余额支付表ID */
    private Long id;

    /** 汇付天下申请编号 */
    @Excel(name = "汇付天下系统编号")
    private String sysId;

    /** 汇付天下申请编号 */
    @Excel(name = "汇付天下申请编号")
    private String reqSeqId;

    /** 提现金额 */
    @Excel(name = "提现金额")
    private String cashAmt;

    /** 取现方ID号 */
    @Excel(name = "取现方ID号")
    private String huifuId;

    /** 取现卡序列号 */
    @Excel(name = "取现卡序列号")
    private String tokenNo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSysId(String sysId) 
    {
        this.sysId = sysId;
    }

    public String getSysId() 
    {
        return sysId;
    }
    public void setReqSeqId(String reqSeqId) 
    {
        this.reqSeqId = reqSeqId;
    }

    public String getReqSeqId() 
    {
        return reqSeqId;
    }
    public void setCashAmt(String cashAmt) 
    {
        this.cashAmt = cashAmt;
    }

    public String getCashAmt() 
    {
        return cashAmt;
    }
    public void setHuifuId(String huifuId) 
    {
        this.huifuId = huifuId;
    }

    public String getHuifuId() 
    {
        return huifuId;
    }
    public void setTokenNo(String tokenNo) 
    {
        this.tokenNo = tokenNo;
    }

    public String getTokenNo() 
    {
        return tokenNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sysId", getSysId())
            .append("reqSeqId", getReqSeqId())
            .append("cashAmt", getCashAmt())
            .append("huifuId", getHuifuId())
            .append("tokenNo", getTokenNo())
            .append("createTime", getCreateTime())
            .toString();
    }
}
