package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.HuifutianxiaUserBank;

/**
 * 汇付天下用户业务入驻Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-14
 */
public interface HuifutianxiaUserBankMapper 
{
    /**
     * 查询汇付天下用户业务入驻
     * 
     * @param id 汇付天下用户业务入驻主键
     * @return 汇付天下用户业务入驻
     */
    public HuifutianxiaUserBank selectHuifutianxiaUserBankById(Long id);

    /**
     * 查询汇付天下用户业务入驻列表
     * 
     * @param huifutianxiaUserBank 汇付天下用户业务入驻
     * @return 汇付天下用户业务入驻集合
     */
    public List<HuifutianxiaUserBank> selectHuifutianxiaUserBankList(HuifutianxiaUserBank huifutianxiaUserBank);

    /**
     * 新增汇付天下用户业务入驻
     * 
     * @param huifutianxiaUserBank 汇付天下用户业务入驻
     * @return 结果
     */
    public int insertHuifutianxiaUserBank(HuifutianxiaUserBank huifutianxiaUserBank);

    /**
     * 修改汇付天下用户业务入驻
     * 
     * @param huifutianxiaUserBank 汇付天下用户业务入驻
     * @return 结果
     */
    public int updateHuifutianxiaUserBank(HuifutianxiaUserBank huifutianxiaUserBank);

    /**
     * 删除汇付天下用户业务入驻
     * 
     * @param id 汇付天下用户业务入驻主键
     * @return 结果
     */
    public int deleteHuifutianxiaUserBankById(Long id);

    /**
     * 批量删除汇付天下用户业务入驻
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHuifutianxiaUserBankByIds(String[] ids);
}
