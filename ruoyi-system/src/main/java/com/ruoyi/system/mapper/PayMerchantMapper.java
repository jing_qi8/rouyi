package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.PayMerchant;

/**
 * 支付商户信息Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-06
 */
public interface PayMerchantMapper 
{
    /**
     * 查询支付商户信息
     * 
     * @param id 支付商户信息主键
     * @return 支付商户信息
     */
    public PayMerchant selectPayMerchantById(Long id);

    /**
     * 查询支付商户信息列表
     * 
     * @param payMerchant 支付商户信息
     * @return 支付商户信息集合
     */
    public List<PayMerchant> selectPayMerchantList(PayMerchant payMerchant);

    /**
     * 新增支付商户信息
     * 
     * @param payMerchant 支付商户信息
     * @return 结果
     */
    public int insertPayMerchant(PayMerchant payMerchant);

    /**
     * 修改支付商户信息
     * 
     * @param payMerchant 支付商户信息
     * @return 结果
     */
    public int updatePayMerchant(PayMerchant payMerchant);

    /**
     * 删除支付商户信息
     * 
     * @param id 支付商户信息主键
     * @return 结果
     */
    public int deletePayMerchantById(Long id);

    /**
     * 批量删除支付商户信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePayMerchantByIds(String[] ids);
}
