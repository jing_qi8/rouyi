package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.HuifutianxiaYuePay;

/**
 * 汇付天下余额支付Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
public interface HuifutianxiaYuePayMapper 
{
    /**
     * 查询汇付天下余额支付
     * 
     * @param id 汇付天下余额支付主键
     * @return 汇付天下余额支付
     */
    public HuifutianxiaYuePay selectHuifutianxiaYuePayById(Long id);

    /**
     * 查询汇付天下余额支付列表
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 汇付天下余额支付集合
     */
    public List<HuifutianxiaYuePay> selectHuifutianxiaYuePayList(HuifutianxiaYuePay huifutianxiaYuePay);

    /**
     * 新增汇付天下余额支付
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 结果
     */
    public int insertHuifutianxiaYuePay(HuifutianxiaYuePay huifutianxiaYuePay);

    /**
     * 修改汇付天下余额支付
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 结果
     */
    public int updateHuifutianxiaYuePay(HuifutianxiaYuePay huifutianxiaYuePay);

    /**
     * 删除汇付天下余额支付
     * 
     * @param id 汇付天下余额支付主键
     * @return 结果
     */
    public int deleteHuifutianxiaYuePayById(Long id);

    /**
     * 批量删除汇付天下余额支付
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHuifutianxiaYuePayByIds(String[] ids);
}
