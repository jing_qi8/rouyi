package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.HuifutianxiaEnchashment;

/**
 * 汇付天下提现Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
public interface HuifutianxiaEnchashmentMapper 
{
    /**
     * 查询汇付天下提现
     * 
     * @param id 汇付天下提现主键
     * @return 汇付天下提现
     */
    public HuifutianxiaEnchashment selectHuifutianxiaEnchashmentById(Long id);

    /**
     * 查询汇付天下提现列表
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 汇付天下提现集合
     */
    public List<HuifutianxiaEnchashment> selectHuifutianxiaEnchashmentList(HuifutianxiaEnchashment huifutianxiaEnchashment);

    /**
     * 新增汇付天下提现
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 结果
     */
    public int insertHuifutianxiaEnchashment(HuifutianxiaEnchashment huifutianxiaEnchashment);

    /**
     * 修改汇付天下提现
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 结果
     */
    public int updateHuifutianxiaEnchashment(HuifutianxiaEnchashment huifutianxiaEnchashment);

    /**
     * 删除汇付天下提现
     * 
     * @param id 汇付天下提现主键
     * @return 结果
     */
    public int deleteHuifutianxiaEnchashmentById(Long id);

    /**
     * 批量删除汇付天下提现
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHuifutianxiaEnchashmentByIds(String[] ids);
}
