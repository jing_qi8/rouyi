package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.HuifutianxiaUserIndv;

/**
 * 个人用户基本信息开户Service接口
 * 
 * @author ruoyi
 * @date 2023-10-14
 */
public interface IHuifutianxiaUserIndvService 
{
    /**
     * 查询个人用户基本信息开户
     * 
     * @param id 个人用户基本信息开户主键
     * @return 个人用户基本信息开户
     */
    public HuifutianxiaUserIndv selectHuifutianxiaUserIndvById(Long id);

    /**
     * 查询个人用户基本信息开户列表
     * 
     * @param huifutianxiaUserIndv 个人用户基本信息开户
     * @return 个人用户基本信息开户集合
     */
    public List<HuifutianxiaUserIndv> selectHuifutianxiaUserIndvList(HuifutianxiaUserIndv huifutianxiaUserIndv);

    /**
     * 新增个人用户基本信息开户
     * 
     * @param huifutianxiaUserIndv 个人用户基本信息开户
     * @return 结果
     */
    public int insertHuifutianxiaUserIndv(HuifutianxiaUserIndv huifutianxiaUserIndv);

    /**
     * 修改个人用户基本信息开户
     * 
     * @param huifutianxiaUserIndv 个人用户基本信息开户
     * @return 结果
     */
    public int updateHuifutianxiaUserIndv(HuifutianxiaUserIndv huifutianxiaUserIndv);

    /**
     * 批量删除个人用户基本信息开户
     * 
     * @param ids 需要删除的个人用户基本信息开户主键集合
     * @return 结果
     */
    public int deleteHuifutianxiaUserIndvByIds(String ids);

    /**
     * 删除个人用户基本信息开户信息
     * 
     * @param id 个人用户基本信息开户主键
     * @return 结果
     */
    public int deleteHuifutianxiaUserIndvById(Long id);
}
