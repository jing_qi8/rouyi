package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.HuifutianxiaEnchashmentMapper;
import com.ruoyi.system.domain.HuifutianxiaEnchashment;
import com.ruoyi.system.service.IHuifutianxiaEnchashmentService;
import com.ruoyi.common.core.text.Convert;

/**
 * 汇付天下提现Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
@Service
public class HuifutianxiaEnchashmentServiceImpl implements IHuifutianxiaEnchashmentService 
{
    @Autowired
    private HuifutianxiaEnchashmentMapper huifutianxiaEnchashmentMapper;

    /**
     * 查询汇付天下提现
     * 
     * @param id 汇付天下提现主键
     * @return 汇付天下提现
     */
    @Override
    public HuifutianxiaEnchashment selectHuifutianxiaEnchashmentById(Long id)
    {
        return huifutianxiaEnchashmentMapper.selectHuifutianxiaEnchashmentById(id);
    }

    /**
     * 查询汇付天下提现列表
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 汇付天下提现
     */
    @Override
    public List<HuifutianxiaEnchashment> selectHuifutianxiaEnchashmentList(HuifutianxiaEnchashment huifutianxiaEnchashment)
    {
        return huifutianxiaEnchashmentMapper.selectHuifutianxiaEnchashmentList(huifutianxiaEnchashment);
    }

    /**
     * 新增汇付天下提现
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 结果
     */
    @Override
    public int insertHuifutianxiaEnchashment(HuifutianxiaEnchashment huifutianxiaEnchashment)
    {
        huifutianxiaEnchashment.setCreateTime(DateUtils.getNowDate());
        return huifutianxiaEnchashmentMapper.insertHuifutianxiaEnchashment(huifutianxiaEnchashment);
    }

    /**
     * 修改汇付天下提现
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 结果
     */
    @Override
    public int updateHuifutianxiaEnchashment(HuifutianxiaEnchashment huifutianxiaEnchashment)
    {
        return huifutianxiaEnchashmentMapper.updateHuifutianxiaEnchashment(huifutianxiaEnchashment);
    }

    /**
     * 批量删除汇付天下提现
     * 
     * @param ids 需要删除的汇付天下提现主键
     * @return 结果
     */
    @Override
    public int deleteHuifutianxiaEnchashmentByIds(String ids)
    {
        return huifutianxiaEnchashmentMapper.deleteHuifutianxiaEnchashmentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除汇付天下提现信息
     * 
     * @param id 汇付天下提现主键
     * @return 结果
     */
    @Override
    public int deleteHuifutianxiaEnchashmentById(Long id)
    {
        return huifutianxiaEnchashmentMapper.deleteHuifutianxiaEnchashmentById(id);
    }
}
