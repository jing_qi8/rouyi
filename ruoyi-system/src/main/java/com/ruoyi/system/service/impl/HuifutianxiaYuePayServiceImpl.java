package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.HuifutianxiaYuePayMapper;
import com.ruoyi.system.domain.HuifutianxiaYuePay;
import com.ruoyi.system.service.IHuifutianxiaYuePayService;
import com.ruoyi.common.core.text.Convert;

/**
 * 汇付天下余额支付Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
@Service
public class HuifutianxiaYuePayServiceImpl implements IHuifutianxiaYuePayService 
{
    @Autowired
    private HuifutianxiaYuePayMapper huifutianxiaYuePayMapper;

    /**
     * 查询汇付天下余额支付
     * 
     * @param id 汇付天下余额支付主键
     * @return 汇付天下余额支付
     */
    @Override
    public HuifutianxiaYuePay selectHuifutianxiaYuePayById(Long id)
    {
        return huifutianxiaYuePayMapper.selectHuifutianxiaYuePayById(id);
    }

    /**
     * 查询汇付天下余额支付列表
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 汇付天下余额支付
     */
    @Override
    public List<HuifutianxiaYuePay> selectHuifutianxiaYuePayList(HuifutianxiaYuePay huifutianxiaYuePay)
    {
        return huifutianxiaYuePayMapper.selectHuifutianxiaYuePayList(huifutianxiaYuePay);
    }

    /**
     * 新增汇付天下余额支付
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 结果
     */
    @Override
    public int insertHuifutianxiaYuePay(HuifutianxiaYuePay huifutianxiaYuePay)
    {
        huifutianxiaYuePay.setCreateTime(DateUtils.getNowDate());
        return huifutianxiaYuePayMapper.insertHuifutianxiaYuePay(huifutianxiaYuePay);
    }

    /**
     * 修改汇付天下余额支付
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 结果
     */
    @Override
    public int updateHuifutianxiaYuePay(HuifutianxiaYuePay huifutianxiaYuePay)
    {
        return huifutianxiaYuePayMapper.updateHuifutianxiaYuePay(huifutianxiaYuePay);
    }

    /**
     * 批量删除汇付天下余额支付
     * 
     * @param ids 需要删除的汇付天下余额支付主键
     * @return 结果
     */
    @Override
    public int deleteHuifutianxiaYuePayByIds(String ids)
    {
        return huifutianxiaYuePayMapper.deleteHuifutianxiaYuePayByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除汇付天下余额支付信息
     * 
     * @param id 汇付天下余额支付主键
     * @return 结果
     */
    @Override
    public int deleteHuifutianxiaYuePayById(Long id)
    {
        return huifutianxiaYuePayMapper.deleteHuifutianxiaYuePayById(id);
    }
}
