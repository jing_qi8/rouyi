package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PayMerchantMapper;
import com.ruoyi.system.domain.PayMerchant;
import com.ruoyi.system.service.IPayMerchantService;
import com.ruoyi.common.core.text.Convert;

/**
 * 支付商户信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-05
 */
@Service
public class PayMerchantServiceImpl implements IPayMerchantService 
{
    @Autowired
    private PayMerchantMapper payMerchantMapper;

    /**
     * 查询支付商户信息
     * 
     * @param id 支付商户信息主键
     * @return 支付商户信息
     */
    @Override
    public PayMerchant selectPayMerchantById(Long id)
    {
        return payMerchantMapper.selectPayMerchantById(id);
    }

    /**
     * 查询支付商户信息列表
     * 
     * @param payMerchant 支付商户信息
     * @return 支付商户信息
     */
    @Override
    public List<PayMerchant> selectPayMerchantList(PayMerchant payMerchant)
    {
        return payMerchantMapper.selectPayMerchantList(payMerchant);
    }

    /**
     * 新增支付商户信息
     * 
     * @param payMerchant 支付商户信息
     * @return 结果
     */
    @Override
    public int insertPayMerchant(PayMerchant payMerchant)
    {
        payMerchant.setCreateTime(DateUtils.getNowDate());
        return payMerchantMapper.insertPayMerchant(payMerchant);
    }

    /**
     * 修改支付商户信息
     * 
     * @param payMerchant 支付商户信息
     * @return 结果
     */
    @Override
    public int updatePayMerchant(PayMerchant payMerchant)
    {
        payMerchant.setUpdateTime(DateUtils.getNowDate());
        return payMerchantMapper.updatePayMerchant(payMerchant);
    }

    /**
     * 批量删除支付商户信息
     * 
     * @param ids 需要删除的支付商户信息主键
     * @return 结果
     */
    @Override
    public int deletePayMerchantByIds(String ids)
    {
        return payMerchantMapper.deletePayMerchantByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除支付商户信息信息
     * 
     * @param id 支付商户信息主键
     * @return 结果
     */
    @Override
    public int deletePayMerchantById(Long id)
    {
        return payMerchantMapper.deletePayMerchantById(id);
    }
}
