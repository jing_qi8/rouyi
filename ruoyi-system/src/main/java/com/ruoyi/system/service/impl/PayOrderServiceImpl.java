package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PayOrderMapper;
import com.ruoyi.system.domain.PayOrder;
import com.ruoyi.system.service.IPayOrderService;
import com.ruoyi.common.core.text.Convert;

/**
 * 支付订单
Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-30
 */
@Service
public class PayOrderServiceImpl implements IPayOrderService 
{
    @Autowired
    private PayOrderMapper payOrderMapper;

    /**
     * 查询支付订单

     * 
     * @param id 支付订单
主键
     * @return 支付订单

     */
    @Override
    public PayOrder selectPayOrderById(Long id)
    {
        return  payOrderMapper.selectPayOrderById(id);
    }

    /**
     * 查询支付订单
列表
     * 
     * @param payOrder 支付订单

     * @return 支付订单

     */
    @Override
    public List<PayOrder> selectPayOrderList(PayOrder payOrder)
    {
        return payOrderMapper.selectPayOrderList(payOrder);
    }

    /**
     * 新增支付订单

     * 
     * @param payOrder 支付订单

     * @return 结果
     */
    @Override
    public int insertPayOrder(PayOrder payOrder)
    {
        payOrder.setCreateTime(DateUtils.getNowDate());
        return payOrderMapper.insertPayOrder(payOrder);
    }

    /**
     * 修改支付订单

     * 
     * @param payOrder 支付订单

     * @return 结果
     */
    @Override
    public int updatePayOrder(PayOrder payOrder)
    {
        payOrder.setUpdateTime(DateUtils.getNowDate());
        return payOrderMapper.updatePayOrder(payOrder);
    }

    /**
     * 批量删除支付订单

     * 
     * @param ids 需要删除的支付订单
主键
     * @return 结果
     */
    @Override
    public int deletePayOrderByIds(String ids)
    {
        return payOrderMapper.deletePayOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除支付订单
信息
     * 
     * @param id 支付订单
主键
     * @return 结果
     */
    @Override
    public int deletePayOrderById(Long id)
    {
        return payOrderMapper.deletePayOrderById(id);
    }
}
