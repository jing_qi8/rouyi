package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.HuifutianxiaUserIndvMapper;
import com.ruoyi.system.domain.HuifutianxiaUserIndv;
import com.ruoyi.system.service.IHuifutianxiaUserIndvService;
import com.ruoyi.common.core.text.Convert;

/**
 * 个人用户基本信息开户Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-14
 */
@Service
public class HuifutianxiaUserIndvServiceImpl implements IHuifutianxiaUserIndvService 
{
    @Autowired
    private HuifutianxiaUserIndvMapper huifutianxiaUserIndvMapper;

    /**
     * 查询个人用户基本信息开户
     * 
     * @param id 个人用户基本信息开户主键
     * @return 个人用户基本信息开户
     */
    @Override
    public HuifutianxiaUserIndv selectHuifutianxiaUserIndvById(Long id)
    {
        return huifutianxiaUserIndvMapper.selectHuifutianxiaUserIndvById(id);
    }

    /**
     * 查询个人用户基本信息开户列表
     * 
     * @param huifutianxiaUserIndv 个人用户基本信息开户
     * @return 个人用户基本信息开户
     */
    @Override
    public List<HuifutianxiaUserIndv> selectHuifutianxiaUserIndvList(HuifutianxiaUserIndv huifutianxiaUserIndv)
    {
        return huifutianxiaUserIndvMapper.selectHuifutianxiaUserIndvList(huifutianxiaUserIndv);
    }

    /**
     * 新增个人用户基本信息开户
     * 
     * @param huifutianxiaUserIndv 个人用户基本信息开户
     * @return 结果
     */
    @Override
    public int insertHuifutianxiaUserIndv(HuifutianxiaUserIndv huifutianxiaUserIndv)
    {
        huifutianxiaUserIndv.setCreateTime(DateUtils.getNowDate());
        return huifutianxiaUserIndvMapper.insertHuifutianxiaUserIndv(huifutianxiaUserIndv);
    }

    /**
     * 修改个人用户基本信息开户
     * 
     * @param huifutianxiaUserIndv 个人用户基本信息开户
     * @return 结果
     */
    @Override
    public int updateHuifutianxiaUserIndv(HuifutianxiaUserIndv huifutianxiaUserIndv)
    {
        return huifutianxiaUserIndvMapper.updateHuifutianxiaUserIndv(huifutianxiaUserIndv);
    }

    /**
     * 批量删除个人用户基本信息开户
     * 
     * @param ids 需要删除的个人用户基本信息开户主键
     * @return 结果
     */
    @Override
    public int deleteHuifutianxiaUserIndvByIds(String ids)
    {
        return huifutianxiaUserIndvMapper.deleteHuifutianxiaUserIndvByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除个人用户基本信息开户信息
     * 
     * @param id 个人用户基本信息开户主键
     * @return 结果
     */
    @Override
    public int deleteHuifutianxiaUserIndvById(Long id)
    {
        return huifutianxiaUserIndvMapper.deleteHuifutianxiaUserIndvById(id);
    }
}
