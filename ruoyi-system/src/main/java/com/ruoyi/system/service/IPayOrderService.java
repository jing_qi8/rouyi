package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.PayOrder;

/**
 * 支付订单
Service接口
 * 
 * @author ruoyi
 * @date 2023-09-30
 */
public interface IPayOrderService 
{
    /**
     * 查询支付订单

     * 
     * @param id 支付订单
主键
     * @return 支付订单

     */
    public PayOrder selectPayOrderById(Long id);

    /**
     * 查询支付订单
列表
     * 
     * @param payOrder 支付订单

     * @return 支付订单
集合
     */
    public List<PayOrder> selectPayOrderList(PayOrder payOrder);

    /**
     * 新增支付订单

     * 
     * @param payOrder 支付订单

     * @return 结果
     */
    public int insertPayOrder(PayOrder payOrder);

    /**
     * 修改支付订单

     * 
     * @param payOrder 支付订单

     * @return 结果
     */
    public int updatePayOrder(PayOrder payOrder);

    /**
     * 批量删除支付订单

     * 
     * @param ids 需要删除的支付订单
主键集合
     * @return 结果
     */
    public int deletePayOrderByIds(String ids);

    /**
     * 删除支付订单
信息
     * 
     * @param id 支付订单
主键
     * @return 结果
     */
    public int deletePayOrderById(Long id);
}
