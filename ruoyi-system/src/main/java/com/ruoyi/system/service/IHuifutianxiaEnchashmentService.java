package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.HuifutianxiaEnchashment;

/**
 * 汇付天下提现Service接口
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
public interface IHuifutianxiaEnchashmentService 
{
    /**
     * 查询汇付天下提现
     * 
     * @param id 汇付天下提现主键
     * @return 汇付天下提现
     */
    public HuifutianxiaEnchashment selectHuifutianxiaEnchashmentById(Long id);

    /**
     * 查询汇付天下提现列表
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 汇付天下提现集合
     */
    public List<HuifutianxiaEnchashment> selectHuifutianxiaEnchashmentList(HuifutianxiaEnchashment huifutianxiaEnchashment);

    /**
     * 新增汇付天下提现
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 结果
     */
    public int insertHuifutianxiaEnchashment(HuifutianxiaEnchashment huifutianxiaEnchashment);

    /**
     * 修改汇付天下提现
     * 
     * @param huifutianxiaEnchashment 汇付天下提现
     * @return 结果
     */
    public int updateHuifutianxiaEnchashment(HuifutianxiaEnchashment huifutianxiaEnchashment);

    /**
     * 批量删除汇付天下提现
     * 
     * @param ids 需要删除的汇付天下提现主键集合
     * @return 结果
     */
    public int deleteHuifutianxiaEnchashmentByIds(String ids);

    /**
     * 删除汇付天下提现信息
     * 
     * @param id 汇付天下提现主键
     * @return 结果
     */
    public int deleteHuifutianxiaEnchashmentById(Long id);
}
