package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.HuifutianxiaYuePay;

/**
 * 汇付天下余额支付Service接口
 * 
 * @author ruoyi
 * @date 2023-10-15
 */
public interface IHuifutianxiaYuePayService 
{
    /**
     * 查询汇付天下余额支付
     * 
     * @param id 汇付天下余额支付主键
     * @return 汇付天下余额支付
     */
    public HuifutianxiaYuePay selectHuifutianxiaYuePayById(Long id);

    /**
     * 查询汇付天下余额支付列表
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 汇付天下余额支付集合
     */
    public List<HuifutianxiaYuePay> selectHuifutianxiaYuePayList(HuifutianxiaYuePay huifutianxiaYuePay);

    /**
     * 新增汇付天下余额支付
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 结果
     */
    public int insertHuifutianxiaYuePay(HuifutianxiaYuePay huifutianxiaYuePay);

    /**
     * 修改汇付天下余额支付
     * 
     * @param huifutianxiaYuePay 汇付天下余额支付
     * @return 结果
     */
    public int updateHuifutianxiaYuePay(HuifutianxiaYuePay huifutianxiaYuePay);

    /**
     * 批量删除汇付天下余额支付
     * 
     * @param ids 需要删除的汇付天下余额支付主键集合
     * @return 结果
     */
    public int deleteHuifutianxiaYuePayByIds(String ids);

    /**
     * 删除汇付天下余额支付信息
     * 
     * @param id 汇付天下余额支付主键
     * @return 结果
     */
    public int deleteHuifutianxiaYuePayById(Long id);
}
