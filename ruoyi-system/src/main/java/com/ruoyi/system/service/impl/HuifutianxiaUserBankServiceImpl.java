package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.HuifutianxiaUserBankMapper;
import com.ruoyi.system.domain.HuifutianxiaUserBank;
import com.ruoyi.system.service.IHuifutianxiaUserBankService;
import com.ruoyi.common.core.text.Convert;

/**
 * 汇付天下用户业务入驻Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-14
 */
@Service
public class HuifutianxiaUserBankServiceImpl implements IHuifutianxiaUserBankService 
{
    @Autowired
    private HuifutianxiaUserBankMapper huifutianxiaUserBankMapper;

    /**
     * 查询汇付天下用户业务入驻
     * 
     * @param id 汇付天下用户业务入驻主键
     * @return 汇付天下用户业务入驻
     */
    @Override
    public HuifutianxiaUserBank selectHuifutianxiaUserBankById(Long id)
    {
        return huifutianxiaUserBankMapper.selectHuifutianxiaUserBankById(id);
    }

    /**
     * 查询汇付天下用户业务入驻列表
     * 
     * @param huifutianxiaUserBank 汇付天下用户业务入驻
     * @return 汇付天下用户业务入驻
     */
    @Override
    public List<HuifutianxiaUserBank> selectHuifutianxiaUserBankList(HuifutianxiaUserBank huifutianxiaUserBank)
    {
        return huifutianxiaUserBankMapper.selectHuifutianxiaUserBankList(huifutianxiaUserBank);
    }

    /**
     * 新增汇付天下用户业务入驻
     * 
     * @param huifutianxiaUserBank 汇付天下用户业务入驻
     * @return 结果
     */
    @Override
    public int insertHuifutianxiaUserBank(HuifutianxiaUserBank huifutianxiaUserBank)
    {
        huifutianxiaUserBank.setCreateTime(DateUtils.getNowDate());
        return huifutianxiaUserBankMapper.insertHuifutianxiaUserBank(huifutianxiaUserBank);
    }

    /**
     * 修改汇付天下用户业务入驻
     * 
     * @param huifutianxiaUserBank 汇付天下用户业务入驻
     * @return 结果
     */
    @Override
    public int updateHuifutianxiaUserBank(HuifutianxiaUserBank huifutianxiaUserBank)
    {
        return huifutianxiaUserBankMapper.updateHuifutianxiaUserBank(huifutianxiaUserBank);
    }

    /**
     * 批量删除汇付天下用户业务入驻
     * 
     * @param ids 需要删除的汇付天下用户业务入驻主键
     * @return 结果
     */
    @Override
    public int deleteHuifutianxiaUserBankByIds(String ids)
    {
        return huifutianxiaUserBankMapper.deleteHuifutianxiaUserBankByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除汇付天下用户业务入驻信息
     * 
     * @param id 汇付天下用户业务入驻主键
     * @return 结果
     */
    @Override
    public int deleteHuifutianxiaUserBankById(Long id)
    {
        return huifutianxiaUserBankMapper.deleteHuifutianxiaUserBankById(id);
    }
}
