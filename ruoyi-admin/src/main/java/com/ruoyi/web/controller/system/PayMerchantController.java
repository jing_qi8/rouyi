package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.PayMerchant;
import com.ruoyi.system.service.IPayMerchantService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 支付商户信息Controller
 * 
 * @author ruoyi
 * @date 2023-10-05
 */
@Controller
@RequestMapping("/system/merchant")
public class PayMerchantController extends BaseController
{
    private String prefix = "system/merchant";

    @Autowired
    private IPayMerchantService payMerchantService;

    @RequiresPermissions("system:merchant:view")
    @GetMapping()
    public String merchant()
    {
        return prefix + "/merchant";
    }

    /**
     * 查询支付商户信息列表
     */
    @RequiresPermissions("system:merchant:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PayMerchant payMerchant)
    {
        startPage();
        List<PayMerchant> list = payMerchantService.selectPayMerchantList(payMerchant);
        return getDataTable(list);
    }

    /**
     * 导出支付商户信息列表
     */
    @RequiresPermissions("system:merchant:export")
    @Log(title = "支付商户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PayMerchant payMerchant)
    {
        List<PayMerchant> list = payMerchantService.selectPayMerchantList(payMerchant);
        ExcelUtil<PayMerchant> util = new ExcelUtil<PayMerchant>(PayMerchant.class);
        return util.exportExcel(list, "支付商户信息数据");
    }

    /**
     * 新增支付商户信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存支付商户信息
     */
    @RequiresPermissions("system:merchant:add")
    @Log(title = "支付商户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PayMerchant payMerchant)
    {
        return toAjax(payMerchantService.insertPayMerchant(payMerchant));
    }

    /**
     * 修改支付商户信息
     */
    @RequiresPermissions("system:merchant:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PayMerchant payMerchant = payMerchantService.selectPayMerchantById(id);
        mmap.put("payMerchant", payMerchant);
        return prefix + "/edit";
    }

    /**
     * 修改保存支付商户信息
     */
    @RequiresPermissions("system:merchant:edit")
    @Log(title = "支付商户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PayMerchant payMerchant)
    {
        return toAjax(payMerchantService.updatePayMerchant(payMerchant));
    }

    /**
     * 删除支付商户信息
     */
    @RequiresPermissions("system:merchant:remove")
    @Log(title = "支付商户信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(payMerchantService.deletePayMerchantByIds(ids));
    }
}
