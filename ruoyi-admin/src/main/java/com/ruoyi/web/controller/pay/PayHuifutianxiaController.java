package com.ruoyi.web.controller.pay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.huifutianxia.HttpClientUtils;
import com.ruoyi.common.utils.huifutianxia.JsonUtils;
import com.ruoyi.common.utils.huifutianxia.RsaUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 支付控制台模块
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/pay/huifutianxia")
public class PayHuifutianxiaController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(PayHuifutianxiaController.class);
    //6666000140692979    6666000140692979   XLSISV
    //6666000140917403 PAYUN
    // 6666000140068810  XLSISV
    @Autowired
    private IHuifutianxiaUserBankService huifutianxiaUserBankService;
    @Autowired
    private IHuifutianxiaUserIndvService huifutianxiaUserIndvService;
    @Autowired
    private IHuifutianxiaYuePayService huifutianxiaYuePayService;
    @Autowired
    private IHuifutianxiaEnchashmentService huifutianxiaEnchashmentService;
    private static final String FILE_DELIMETER = ",";
    private String prefix = "huifutianxia";

    /**
     * 用户余额查询跳转
     */
    @GetMapping("/userQueryList")
    public String userQueryList()
    {
        return prefix + "/userQuery";
    }

    /**
     * 查询个人用户基本信息开户查询
     */
    @PostMapping("/userQueryList")
    @ResponseBody
    public TableDataInfo userQueryList(String huifuId)throws Exception
    {
        if(huifuId.isEmpty()){
            return getDataTable(new JSONArray());
        }
        String privateKey ="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCRzZ0q2ycJkDPuJ24CthLQzfn2pjc2Q1yMAUFFkHwcc0zhZXlsGp+CQdR4cSrm55MlEJjtQzK4eTh/cDdEy3Ne3Ju9u14zTZqWzNMPOVa05tJ6xIjQj9dVxTBZ3jwP8qqDcdF8/TU1tfP2rCeFKcrKufPbyNZq9jUZj1r8v1mJlvLTXyzyCvzdET8szaeUcNh8QYV7VaGNGyUipzK0+8wRYqO9Y9xIte8I3zWrHVgz4nThguvBG+HY0hFyMs3u20OoG2dDNq/81rAQtSSdE6m+BSZgqLY7f8VGAA5Q74zLH4GU3sNOW3TJyxXgKRa0K8Mzw3THsMuEdgxeIK5dG1jjAgMBAAECggEAfDEmsY1wh/3TrgTZ+Oc7ya3ZP9W+67KfDwY2odl7TgSMNOVpcsKOobYPE/RZNcuFE6o6iMaXWniuviZIfYnKoL/KzObfS1XL2q1nDe2qRHbkS/xCCVysb/uBvtrEUFVEFqiJZyyK+VAeGTv0gcHiYaHY7Jn6wasr+bSZLPaUftPSlJlSKnzDIF+faE+IkQVceArm/sGtfPCYNcJOZksn/kd3H8FsqufJZePNF6F8SSB127l1j8zsXUWi7glYw2n7xiHzxVjBqYLut4dVOIv3dUBIacka3wYs0ug/bW9rvmSd5lOJmKKGAhRfHRnD2zmFKy/YkqSFnWcZPp724xStCQKBgQDFu3NADnqEaeC0I8c5Lq//c+8KzOkbnQz6A+LrvGiR9zLXHE3F544Mtjr1CbUbobLWPMBgtVDshkJ5t+VWB3r/nxiCjSYj3b02qVzmZq5eicHC0Z5PaLdVpNTLff9ukAndNGO8z+utB+tePOwi8jlYkXswVkZevmyaMS2hwpGr9QKBgQC8xLu/KLKM3tYN/usDN6INElvr5UXnbVHIWNFKlidwJ5IoB7+zUBlCF5Z3nkpqK6/su1pzVxnB6Kh+SodUc32sp0EiTai6AcrwcpNNO2AIn+2bLTA7hcHwQVKnD1lZtzXHZluTUVCweyJgn0bC/IHohNOr3fuBBVHCKh+fQIqCdwKBgEzJEGZtuPI204Xg5vqzwLDo6ok9LcEFagak/7gfwFP+tQWH/kO+OhGBqr+Bd401a+d6TBLCFpzjPmlaGnsgCIm/1JrOCWOgNlxFxMfMVCZuRIpNMLcRqTBfBCvJ0Cm1Ub9PGvQ1ogXdr283JVQk0FQumrIYrtcYB8CRHHFWx48JAoGBAKDKs2HROMXlyk+BpI3JBbA8wy/cy/6lj9QoU55E6LMQAd83OoNy5xD1JqYdnyVwsRnlPNaiir4xf9STh1LYTWST01d07xQCEutdrTtMKDEwiSR1CXCqLtiHy++hk2poNNb0yWxjB2hMeqvzAEN28vEss2DiZSkxbsMFPopYX6NhAoGBAIH5oBKUFDE3YFlzrkrTcEVyHf/KeLdeor8oLkTCyzFak9yMVqX36UCns2ch9R5zToXajtOoXNJvUoaD+zdznnDgzfLY3BsXiJz+VzjyxqnoDn1F1tSdZV1+lG3eaKZy7ATQHuiF4CxTFGILlnaBX3D6/JokUvf3JTXoSXIJXGc7";
        Map<String, String> headers = new HashMap<>(4);
        headers.put("Content-type", "application/json");
        headers.put("sdk_version", "javaSDK_" + "3.0.5");

        Map<String, Object> params = new HashMap<>();
        params.put("huifu_id", huifuId);
        Random rand = new Random();
        int num=rand.nextInt(100)+1;
        params.put("req_seq_id", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
        params.put("req_date", new SimpleDateFormat("yyyyMMdd").format(new Date()).toString());
        String reqData = JSONObject.toJSONString(params);
        String sortedData = JsonUtils.sort4JsonString(reqData, 0);
        String requestSign = RsaUtils.sign(sortedData,privateKey);
        Map<String, Object> request = new HashMap<>();
        request.put("sys_id", "6666000140068810");
        request.put("data", JSONObject.parseObject(reqData));
        request.put("sign", requestSign);
        request.put("product_id", "XLSISV");
        String requestBody = JSON.toJSONString(request);
        log.info("汇付余额查询请求数据"+requestBody);
        String back = HttpClientUtils.httpPostJson("https://api.huifu.com/v2/trade/acctpayment/balance/query".toString(), headers, requestBody);
        log.info("汇付余额查询返回数据"+back);
        Map<String, Object> httpResponseMap = (Map<String, Object>) JSONObject.parse(back);
        Map<String, Object> responseDataMap = (Map<String, Object>) JSONObject.parse(httpResponseMap.get("data").toString());
//        JSONArray dtoList = JSON.parseArray(responseDataMap.get("acctInfo_list").toString());
        if(responseDataMap.get("resp_code").toString().equals("00000000")){
            List<JSONObject> list = JSONObject.parseArray(responseDataMap.get("acctInfo_list").toString(),JSONObject.class);;
            startPage();
            return getDataTable(list);
        }else{
            return getDataTable(new JSONArray());
        }
        //List<HuifutianxiaUserIndv> list = huifutianxiaUserIndvService.selectHuifutianxiaUserIndvList(huifutianxiaUserIndv);

    }

    /**
     * 个人用户基本信息开户查询跳转
     */
    @GetMapping("/indvList")
    public String orderList(){

        return prefix + "/indv";
    }

    /**
     * 查询个人用户基本信息开户查询
     */
    @PostMapping("/indvList")
    @ResponseBody
    public TableDataInfo indvList(HuifutianxiaUserIndv huifutianxiaUserIndv)
    {
        // 取身份信息
//        SysUser user = getSysUser();
//        huifutianxiaUserIndv.setMerchantId(user.getRemark());
        startPage();
        List<HuifutianxiaUserIndv> list = huifutianxiaUserIndvService.selectHuifutianxiaUserIndvList(huifutianxiaUserIndv);
        return getDataTable(list);
    }
    /**
     * 新增人用户基本信息开户
     */
    @GetMapping("/indvAdd")
    public String add()
    {
        return prefix + "/indvAdd";
    }

    /**
     * 新增保存个人用户基本信息开户
     */

    @Log(title = "个人用户基本信息开户", businessType = BusinessType.INSERT)
    @PostMapping("/indvAdd")
    @ResponseBody
    public AjaxResult indvAddSave(HuifutianxiaUserIndv huifutianxiaUserIndv)throws Exception
    {
        Random rand = new Random();
        int num=rand.nextInt(100)+1;
        huifutianxiaUserIndv.setMerchantId("6666000140068810");
        huifutianxiaUserIndv.setReqSeqId("rQ"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
        String privateKey ="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCRzZ0q2ycJkDPuJ24CthLQzfn2pjc2Q1yMAUFFkHwcc0zhZXlsGp+CQdR4cSrm55MlEJjtQzK4eTh/cDdEy3Ne3Ju9u14zTZqWzNMPOVa05tJ6xIjQj9dVxTBZ3jwP8qqDcdF8/TU1tfP2rCeFKcrKufPbyNZq9jUZj1r8v1mJlvLTXyzyCvzdET8szaeUcNh8QYV7VaGNGyUipzK0+8wRYqO9Y9xIte8I3zWrHVgz4nThguvBG+HY0hFyMs3u20OoG2dDNq/81rAQtSSdE6m+BSZgqLY7f8VGAA5Q74zLH4GU3sNOW3TJyxXgKRa0K8Mzw3THsMuEdgxeIK5dG1jjAgMBAAECggEAfDEmsY1wh/3TrgTZ+Oc7ya3ZP9W+67KfDwY2odl7TgSMNOVpcsKOobYPE/RZNcuFE6o6iMaXWniuviZIfYnKoL/KzObfS1XL2q1nDe2qRHbkS/xCCVysb/uBvtrEUFVEFqiJZyyK+VAeGTv0gcHiYaHY7Jn6wasr+bSZLPaUftPSlJlSKnzDIF+faE+IkQVceArm/sGtfPCYNcJOZksn/kd3H8FsqufJZePNF6F8SSB127l1j8zsXUWi7glYw2n7xiHzxVjBqYLut4dVOIv3dUBIacka3wYs0ug/bW9rvmSd5lOJmKKGAhRfHRnD2zmFKy/YkqSFnWcZPp724xStCQKBgQDFu3NADnqEaeC0I8c5Lq//c+8KzOkbnQz6A+LrvGiR9zLXHE3F544Mtjr1CbUbobLWPMBgtVDshkJ5t+VWB3r/nxiCjSYj3b02qVzmZq5eicHC0Z5PaLdVpNTLff9ukAndNGO8z+utB+tePOwi8jlYkXswVkZevmyaMS2hwpGr9QKBgQC8xLu/KLKM3tYN/usDN6INElvr5UXnbVHIWNFKlidwJ5IoB7+zUBlCF5Z3nkpqK6/su1pzVxnB6Kh+SodUc32sp0EiTai6AcrwcpNNO2AIn+2bLTA7hcHwQVKnD1lZtzXHZluTUVCweyJgn0bC/IHohNOr3fuBBVHCKh+fQIqCdwKBgEzJEGZtuPI204Xg5vqzwLDo6ok9LcEFagak/7gfwFP+tQWH/kO+OhGBqr+Bd401a+d6TBLCFpzjPmlaGnsgCIm/1JrOCWOgNlxFxMfMVCZuRIpNMLcRqTBfBCvJ0Cm1Ub9PGvQ1ogXdr283JVQk0FQumrIYrtcYB8CRHHFWx48JAoGBAKDKs2HROMXlyk+BpI3JBbA8wy/cy/6lj9QoU55E6LMQAd83OoNy5xD1JqYdnyVwsRnlPNaiir4xf9STh1LYTWST01d07xQCEutdrTtMKDEwiSR1CXCqLtiHy++hk2poNNb0yWxjB2hMeqvzAEN28vEss2DiZSkxbsMFPopYX6NhAoGBAIH5oBKUFDE3YFlzrkrTcEVyHf/KeLdeor8oLkTCyzFak9yMVqX36UCns2ch9R5zToXajtOoXNJvUoaD+zdznnDgzfLY3BsXiJz+VzjyxqnoDn1F1tSdZV1+lG3eaKZy7ATQHuiF4CxTFGILlnaBX3D6/JokUvf3JTXoSXIJXGc7";
        Map<String, String> headers = new HashMap<>(4);
        headers.put("Content-type", "application/json");
        headers.put("sdk_version", "javaSDK_" + "3.0.5");

        Map<String, Object> params = new HashMap<>();
        params.put("huifu_id", "6666000140363982");
        params.put("req_seq_id", huifutianxiaUserIndv.getReqSeqId());
        params.put("req_date", new SimpleDateFormat("yyyyMMdd").format(new Date()).toString());
        params.put("upper_huifu_id", "6666000140068810");
        params.put("name", huifutianxiaUserIndv.getName());
        params.put("cert_type", "00");
        params.put("cert_no", huifutianxiaUserIndv.getCertNo());
        params.put("cert_validity_type", "0");
        params.put("cert_begin_date", huifutianxiaUserIndv.getCertBeginDate());
        params.put("cert_end_date", huifutianxiaUserIndv.getCertEndDate());
        params.put("mobile_no", huifutianxiaUserIndv.getMobileNo());
        String reqData = JSONObject.toJSONString(params);
        String sortedData = JsonUtils.sort4JsonString(reqData, 0);
        String requestSign = RsaUtils.sign(sortedData,privateKey);

        Map<String, Object> request = new HashMap<>();
        request.put("sys_id", "6666000140068810");
        request.put("data", JSONObject.parseObject(reqData));
        request.put("sign", requestSign);
        request.put("product_id", "XLSISV");
        String requestBody = JSON.toJSONString(request);
        log.info("汇付个人用户基本信息开户请求数据"+requestBody);
        String back = HttpClientUtils.httpPostJson("https://api.huifu.com/v2/user/basicdata/indv".toString(), headers, requestBody);
        log.info("汇付个人用户基本信息开户返回数据"+back);
        Map<String, Object> httpResponseMap = (Map<String, Object>) JSONObject.parse(back);
        Map<String, Object> responseDataMap = (Map<String, Object>) JSONObject.parse(httpResponseMap.get("data").toString());
        if(responseDataMap.get("resp_code").toString().equals("00000000")){
            huifutianxiaUserIndv.setHuifuId(responseDataMap.get("huifu_id").toString());
            huifutianxiaUserIndvService.insertHuifutianxiaUserIndv(huifutianxiaUserIndv);
            return AjaxResult.success("处理成功");
            
        }else{
            return AjaxResult.error(responseDataMap.get("resp_desc").toString());
        }
    }
    /**
     * 查询用户业务入驻跳转
     */
    @GetMapping("/openUserBankList/{id}")
    public String openUserBankList(ModelMap mmap,@PathVariable("id") String id)
    {
        mmap.put("huifuId",id);
        return prefix + "/bank";
    }

    /**
     * 查询用户业务入驻查询
     */
    @PostMapping("/openUserBankList")
    @ResponseBody
    public TableDataInfo openUserBankList(HuifutianxiaUserBank huifutianxiaUserBank)
    {
        huifutianxiaUserBank.setHuifuId(huifutianxiaUserBank.getHuifuId().split(",")[0]);
        // 取身份信息
//        SysUser user = getSysUser();
//        huifutianxiaUserIndv.setMerchantId(user.getRemark());
        startPage();
        List<HuifutianxiaUserBank> list = huifutianxiaUserBankService.selectHuifutianxiaUserBankList(huifutianxiaUserBank);
        return getDataTable(list);
    }

    /**
     * 新增用户业务入驻
     */
    @GetMapping("/openUserBankAdd/{id}")
    public String openUserBankAdd(@PathVariable("id") String huifuId, ModelMap mmap)
    {
        mmap.put("huifuId", huifuId);
        return prefix + "/bankAdd";
    }

    @Log(title = "用户业务入驻", businessType = BusinessType.INSERT)
    @PostMapping("/openUserBankAdd")
    @ResponseBody
    public AjaxResult openUserBankAddSave(HuifutianxiaUserBank huifutianxiaUserBank)throws Exception
    {
        Random rand = new Random();
        int num=rand.nextInt(100)+1;
        huifutianxiaUserBank.setReqSeqId("rQ"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
        String privateKey ="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCRzZ0q2ycJkDPuJ24CthLQzfn2pjc2Q1yMAUFFkHwcc0zhZXlsGp+CQdR4cSrm55MlEJjtQzK4eTh/cDdEy3Ne3Ju9u14zTZqWzNMPOVa05tJ6xIjQj9dVxTBZ3jwP8qqDcdF8/TU1tfP2rCeFKcrKufPbyNZq9jUZj1r8v1mJlvLTXyzyCvzdET8szaeUcNh8QYV7VaGNGyUipzK0+8wRYqO9Y9xIte8I3zWrHVgz4nThguvBG+HY0hFyMs3u20OoG2dDNq/81rAQtSSdE6m+BSZgqLY7f8VGAA5Q74zLH4GU3sNOW3TJyxXgKRa0K8Mzw3THsMuEdgxeIK5dG1jjAgMBAAECggEAfDEmsY1wh/3TrgTZ+Oc7ya3ZP9W+67KfDwY2odl7TgSMNOVpcsKOobYPE/RZNcuFE6o6iMaXWniuviZIfYnKoL/KzObfS1XL2q1nDe2qRHbkS/xCCVysb/uBvtrEUFVEFqiJZyyK+VAeGTv0gcHiYaHY7Jn6wasr+bSZLPaUftPSlJlSKnzDIF+faE+IkQVceArm/sGtfPCYNcJOZksn/kd3H8FsqufJZePNF6F8SSB127l1j8zsXUWi7glYw2n7xiHzxVjBqYLut4dVOIv3dUBIacka3wYs0ug/bW9rvmSd5lOJmKKGAhRfHRnD2zmFKy/YkqSFnWcZPp724xStCQKBgQDFu3NADnqEaeC0I8c5Lq//c+8KzOkbnQz6A+LrvGiR9zLXHE3F544Mtjr1CbUbobLWPMBgtVDshkJ5t+VWB3r/nxiCjSYj3b02qVzmZq5eicHC0Z5PaLdVpNTLff9ukAndNGO8z+utB+tePOwi8jlYkXswVkZevmyaMS2hwpGr9QKBgQC8xLu/KLKM3tYN/usDN6INElvr5UXnbVHIWNFKlidwJ5IoB7+zUBlCF5Z3nkpqK6/su1pzVxnB6Kh+SodUc32sp0EiTai6AcrwcpNNO2AIn+2bLTA7hcHwQVKnD1lZtzXHZluTUVCweyJgn0bC/IHohNOr3fuBBVHCKh+fQIqCdwKBgEzJEGZtuPI204Xg5vqzwLDo6ok9LcEFagak/7gfwFP+tQWH/kO+OhGBqr+Bd401a+d6TBLCFpzjPmlaGnsgCIm/1JrOCWOgNlxFxMfMVCZuRIpNMLcRqTBfBCvJ0Cm1Ub9PGvQ1ogXdr283JVQk0FQumrIYrtcYB8CRHHFWx48JAoGBAKDKs2HROMXlyk+BpI3JBbA8wy/cy/6lj9QoU55E6LMQAd83OoNy5xD1JqYdnyVwsRnlPNaiir4xf9STh1LYTWST01d07xQCEutdrTtMKDEwiSR1CXCqLtiHy++hk2poNNb0yWxjB2hMeqvzAEN28vEss2DiZSkxbsMFPopYX6NhAoGBAIH5oBKUFDE3YFlzrkrTcEVyHf/KeLdeor8oLkTCyzFak9yMVqX36UCns2ch9R5zToXajtOoXNJvUoaD+zdznnDgzfLY3BsXiJz+VzjyxqnoDn1F1tSdZV1+lG3eaKZy7ATQHuiF4CxTFGILlnaBX3D6/JokUvf3JTXoSXIJXGc7";
        Map<String, String> headers = new HashMap<>(4);
        headers.put("Content-type", "application/json");
        headers.put("sdk_version", "javaSDK_" + "3.0.5");

        JSONObject dto = new JSONObject();
        dto.put("fee_rate", "0.00");
        JSONArray dtoList = new JSONArray();
        dtoList.add(dto);
        Map<String, Object> cash_config = new HashMap<>();
        cash_config.put("fee_rate","0.00");
        Map<String, Object> card_info = new HashMap<>();
        card_info.put("card_type","1");
        card_info.put("card_name",huifutianxiaUserBank.getCardName());//赵星远
        card_info.put("card_no",huifutianxiaUserBank.getCardNo());//6221801210001981507
        card_info.put("prov_id",huifutianxiaUserBank.getProvId());//130000
        card_info.put("area_id",huifutianxiaUserBank.getAreaId());//130100
//        card_info.put("cert_type", "00");
//        card_info.put("cert_no", "130133198809070010");
//        card_info.put("cert_validity_type", "0");
//        card_info.put("cert_begin_date", "20220816");
//        card_info.put("cert_end_date", "20420816");
//        card_info.put("mobile_no", "13081102695");

        Map<String, Object> params = new HashMap<>();
        params.put("huifu_id", huifutianxiaUserBank.getHuifuId());//6666000141036927
        params.put("req_seq_id", huifutianxiaUserBank.getReqSeqId());
        params.put("req_date", new SimpleDateFormat("yyyyMMdd").format(new Date()).toString());
        params.put("upper_huifu_id", "6666000140068810");
        params.put("card_info",JSONObject.toJSONString(card_info));
        params.put("cash_config",dtoList);
        String reqData = JSONObject.toJSONString(params);
        String sortedData = JsonUtils.sort4JsonString(reqData, 0);
        String requestSign = RsaUtils.sign(sortedData,privateKey);

        Map<String, Object> request = new HashMap<>();
        request.put("sys_id", "6666000140068810");
        request.put("data", JSONObject.parseObject(reqData));
        request.put("sign", requestSign);
        request.put("product_id", "XLSISV");
        String requestBody = JSON.toJSONString(request);
        log.info("汇付用户业务入驻请求数据"+requestBody);
        String back = HttpClientUtils.httpPostJson("https://api.huifu.com/v2/user/busi/open".toString(), headers, requestBody);
        log.info("汇付用户业务入驻返回数据"+back);
        Map<String, Object> httpResponseMap = (Map<String, Object>) JSONObject.parse(back);
        Map<String, Object> responseDataMap = (Map<String, Object>) JSONObject.parse(httpResponseMap.get("data").toString());
        if(responseDataMap.get("resp_code").toString().equals("00000000")){
            huifutianxiaUserBank.setHuifuId(responseDataMap.get("huifu_id").toString());
            huifutianxiaUserBank.setTokenNo(responseDataMap.get("token_no").toString());
            huifutianxiaUserBankService.insertHuifutianxiaUserBank(huifutianxiaUserBank);
            return AjaxResult.success("处理成功");
        }else{
            return AjaxResult.error(responseDataMap.get("resp_desc").toString());
        }
    }

    /**
     * 余额支付查询跳转
     */
    @GetMapping("/yuePayList")
    public String yuePayList()
    {

        return prefix + "/yuePay";
    }

    /**
     * 查询余额支付查询
     */
    @PostMapping("/yuePayList")
    @ResponseBody
    public TableDataInfo yuePayList(HuifutianxiaYuePay huifutianxiaYuePay)
    {
        // 取身份信息
//        SysUser user = getSysUser();
//        huifutianxiaUserIndv.setMerchantId(user.getRemark());
        startPage();
        List<HuifutianxiaYuePay> list = huifutianxiaYuePayService.selectHuifutianxiaYuePayList(huifutianxiaYuePay);
        return getDataTable(list);
    }

    /**
     * 新增余额支付
     */
    @GetMapping("/yuePayAdd")
    public String yuePayAdd()
    {
        return prefix + "/yuePayAdd";
    }

    /**
     * 新增保存余额支付
     */

    @Log(title = "余额支付", businessType = BusinessType.INSERT)
    @PostMapping("/yuePayAdd")
    @ResponseBody
    public AjaxResult yuePayAddSave(HuifutianxiaYuePay huifutianxiaYuePay)throws Exception
    {
        String privateKey ="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCRzZ0q2ycJkDPuJ24CthLQzfn2pjc2Q1yMAUFFkHwcc0zhZXlsGp+CQdR4cSrm55MlEJjtQzK4eTh/cDdEy3Ne3Ju9u14zTZqWzNMPOVa05tJ6xIjQj9dVxTBZ3jwP8qqDcdF8/TU1tfP2rCeFKcrKufPbyNZq9jUZj1r8v1mJlvLTXyzyCvzdET8szaeUcNh8QYV7VaGNGyUipzK0+8wRYqO9Y9xIte8I3zWrHVgz4nThguvBG+HY0hFyMs3u20OoG2dDNq/81rAQtSSdE6m+BSZgqLY7f8VGAA5Q74zLH4GU3sNOW3TJyxXgKRa0K8Mzw3THsMuEdgxeIK5dG1jjAgMBAAECggEAfDEmsY1wh/3TrgTZ+Oc7ya3ZP9W+67KfDwY2odl7TgSMNOVpcsKOobYPE/RZNcuFE6o6iMaXWniuviZIfYnKoL/KzObfS1XL2q1nDe2qRHbkS/xCCVysb/uBvtrEUFVEFqiJZyyK+VAeGTv0gcHiYaHY7Jn6wasr+bSZLPaUftPSlJlSKnzDIF+faE+IkQVceArm/sGtfPCYNcJOZksn/kd3H8FsqufJZePNF6F8SSB127l1j8zsXUWi7glYw2n7xiHzxVjBqYLut4dVOIv3dUBIacka3wYs0ug/bW9rvmSd5lOJmKKGAhRfHRnD2zmFKy/YkqSFnWcZPp724xStCQKBgQDFu3NADnqEaeC0I8c5Lq//c+8KzOkbnQz6A+LrvGiR9zLXHE3F544Mtjr1CbUbobLWPMBgtVDshkJ5t+VWB3r/nxiCjSYj3b02qVzmZq5eicHC0Z5PaLdVpNTLff9ukAndNGO8z+utB+tePOwi8jlYkXswVkZevmyaMS2hwpGr9QKBgQC8xLu/KLKM3tYN/usDN6INElvr5UXnbVHIWNFKlidwJ5IoB7+zUBlCF5Z3nkpqK6/su1pzVxnB6Kh+SodUc32sp0EiTai6AcrwcpNNO2AIn+2bLTA7hcHwQVKnD1lZtzXHZluTUVCweyJgn0bC/IHohNOr3fuBBVHCKh+fQIqCdwKBgEzJEGZtuPI204Xg5vqzwLDo6ok9LcEFagak/7gfwFP+tQWH/kO+OhGBqr+Bd401a+d6TBLCFpzjPmlaGnsgCIm/1JrOCWOgNlxFxMfMVCZuRIpNMLcRqTBfBCvJ0Cm1Ub9PGvQ1ogXdr283JVQk0FQumrIYrtcYB8CRHHFWx48JAoGBAKDKs2HROMXlyk+BpI3JBbA8wy/cy/6lj9QoU55E6LMQAd83OoNy5xD1JqYdnyVwsRnlPNaiir4xf9STh1LYTWST01d07xQCEutdrTtMKDEwiSR1CXCqLtiHy++hk2poNNb0yWxjB2hMeqvzAEN28vEss2DiZSkxbsMFPopYX6NhAoGBAIH5oBKUFDE3YFlzrkrTcEVyHf/KeLdeor8oLkTCyzFak9yMVqX36UCns2ch9R5zToXajtOoXNJvUoaD+zdznnDgzfLY3BsXiJz+VzjyxqnoDn1F1tSdZV1+lG3eaKZy7ATQHuiF4CxTFGILlnaBX3D6/JokUvf3JTXoSXIJXGc7";
        Map<String, String> headers = new HashMap<>(4);
        headers.put("Content-type", "application/json");
        headers.put("sdk_version", "javaSDK_" + "3.0.5");

        Map<String, Object> params = new HashMap<>();
        Random rand = new Random();
        int num=rand.nextInt(100)+1;
        huifutianxiaYuePay.setReqSeqId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
        params.put("req_seq_id", huifutianxiaYuePay.getReqSeqId());
        params.put("req_date", new SimpleDateFormat("yyyyMMdd").format(new Date()).toString());
        params.put("out_huifu_id", "6666000140068810");
        huifutianxiaYuePay.setOutHuifuId("6666000140068810");
        params.put("ord_amt", huifutianxiaYuePay.getOrdAmt());
        Map<String, Object> acct_split_bunch = new HashMap<>();
        JSONObject dto = new JSONObject();
        dto.put("div_amt", huifutianxiaYuePay.getDivAmt());// 分账金额
        dto.put("huifu_id", huifutianxiaYuePay.getHuifuId());// 被分账方ID
        JSONArray dtoList = new JSONArray();
        dtoList.add(dto);
        acct_split_bunch.put("acct_infos",dtoList);

        Map<String, Object> risk_check_data = new HashMap<>();
        risk_check_data.put("transfer_type","04");
        params.put("acct_split_bunch",JSONObject.toJSONString(acct_split_bunch));
        params.put("risk_check_data",JSONObject.toJSONString(risk_check_data));

        String reqData = JSONObject.toJSONString(params);
        String sortedData = JsonUtils.sort4JsonString(reqData, 0);
        String requestSign = RsaUtils.sign(sortedData,privateKey);
        Map<String, Object> request = new HashMap<>();
        request.put("sys_id", "6666000140068810");
        request.put("data", JSONObject.parseObject(reqData));
        request.put("sign", requestSign);
        request.put("product_id", "XLSISV");
        String requestBody = JSON.toJSONString(request);
        log.info("汇付余额支付请求数据"+requestBody);
        String back = HttpClientUtils.httpPostJson("https://api.huifu.com/v2/trade/acctpayment/pay".toString(), headers, requestBody);
        log.info("汇付余额支付返回数据"+back);
        Map<String, Object> httpResponseMap = (Map<String, Object>) JSONObject.parse(back);
        Map<String, Object> responseDataMap = (Map<String, Object>) JSONObject.parse(httpResponseMap.get("data").toString());
        if(responseDataMap.get("resp_code").toString().equals("00000000")){
            huifutianxiaYuePayService.insertHuifutianxiaYuePay(huifutianxiaYuePay);
            return AjaxResult.success("处理成功");
        }else{
            return AjaxResult.error(responseDataMap.get("resp_desc").toString());
        }
    }

    /**
     * 提现查询跳转
     */
    @GetMapping("/enchashmentList")
    public String enchashmentList()
    {

        return prefix + "/enchashment";
    }

    /**
     * 查询提现查询
     */
    @PostMapping("/enchashmentList")
    @ResponseBody
    public TableDataInfo enchashmentList(HuifutianxiaEnchashment huifutianxiaEnchashment)
    {
        // 取身份信息
//        SysUser user = getSysUser();
//        huifutianxiaUserIndv.setMerchantId(user.getRemark());
        startPage();
        List<HuifutianxiaEnchashment> list = huifutianxiaEnchashmentService.selectHuifutianxiaEnchashmentList(huifutianxiaEnchashment);
        return getDataTable(list);
    }

    /**
     * 新增提现
     */
    @GetMapping("/enchashmentAdd")
    public String enchashmentAdd()
    {
        return prefix + "/enchashmentAdd";
    }

    /**
     * 新增提现
     */

    @Log(title = "提现", businessType = BusinessType.INSERT)
    @PostMapping("/enchashmentAdd")
    @ResponseBody
    public AjaxResult enchashmentAddSave(HuifutianxiaEnchashment huifutianxiaEnchashment)throws Exception
    {
        String privateKey ="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCRzZ0q2ycJkDPuJ24CthLQzfn2pjc2Q1yMAUFFkHwcc0zhZXlsGp+CQdR4cSrm55MlEJjtQzK4eTh/cDdEy3Ne3Ju9u14zTZqWzNMPOVa05tJ6xIjQj9dVxTBZ3jwP8qqDcdF8/TU1tfP2rCeFKcrKufPbyNZq9jUZj1r8v1mJlvLTXyzyCvzdET8szaeUcNh8QYV7VaGNGyUipzK0+8wRYqO9Y9xIte8I3zWrHVgz4nThguvBG+HY0hFyMs3u20OoG2dDNq/81rAQtSSdE6m+BSZgqLY7f8VGAA5Q74zLH4GU3sNOW3TJyxXgKRa0K8Mzw3THsMuEdgxeIK5dG1jjAgMBAAECggEAfDEmsY1wh/3TrgTZ+Oc7ya3ZP9W+67KfDwY2odl7TgSMNOVpcsKOobYPE/RZNcuFE6o6iMaXWniuviZIfYnKoL/KzObfS1XL2q1nDe2qRHbkS/xCCVysb/uBvtrEUFVEFqiJZyyK+VAeGTv0gcHiYaHY7Jn6wasr+bSZLPaUftPSlJlSKnzDIF+faE+IkQVceArm/sGtfPCYNcJOZksn/kd3H8FsqufJZePNF6F8SSB127l1j8zsXUWi7glYw2n7xiHzxVjBqYLut4dVOIv3dUBIacka3wYs0ug/bW9rvmSd5lOJmKKGAhRfHRnD2zmFKy/YkqSFnWcZPp724xStCQKBgQDFu3NADnqEaeC0I8c5Lq//c+8KzOkbnQz6A+LrvGiR9zLXHE3F544Mtjr1CbUbobLWPMBgtVDshkJ5t+VWB3r/nxiCjSYj3b02qVzmZq5eicHC0Z5PaLdVpNTLff9ukAndNGO8z+utB+tePOwi8jlYkXswVkZevmyaMS2hwpGr9QKBgQC8xLu/KLKM3tYN/usDN6INElvr5UXnbVHIWNFKlidwJ5IoB7+zUBlCF5Z3nkpqK6/su1pzVxnB6Kh+SodUc32sp0EiTai6AcrwcpNNO2AIn+2bLTA7hcHwQVKnD1lZtzXHZluTUVCweyJgn0bC/IHohNOr3fuBBVHCKh+fQIqCdwKBgEzJEGZtuPI204Xg5vqzwLDo6ok9LcEFagak/7gfwFP+tQWH/kO+OhGBqr+Bd401a+d6TBLCFpzjPmlaGnsgCIm/1JrOCWOgNlxFxMfMVCZuRIpNMLcRqTBfBCvJ0Cm1Ub9PGvQ1ogXdr283JVQk0FQumrIYrtcYB8CRHHFWx48JAoGBAKDKs2HROMXlyk+BpI3JBbA8wy/cy/6lj9QoU55E6LMQAd83OoNy5xD1JqYdnyVwsRnlPNaiir4xf9STh1LYTWST01d07xQCEutdrTtMKDEwiSR1CXCqLtiHy++hk2poNNb0yWxjB2hMeqvzAEN28vEss2DiZSkxbsMFPopYX6NhAoGBAIH5oBKUFDE3YFlzrkrTcEVyHf/KeLdeor8oLkTCyzFak9yMVqX36UCns2ch9R5zToXajtOoXNJvUoaD+zdznnDgzfLY3BsXiJz+VzjyxqnoDn1F1tSdZV1+lG3eaKZy7ATQHuiF4CxTFGILlnaBX3D6/JokUvf3JTXoSXIJXGc7";
        Map<String, String> headers = new HashMap<>(4);
        headers.put("Content-type", "application/json");
        headers.put("sdk_version", "javaSDK_" + "3.0.5");
        Map<String, Object> params = new HashMap<>();
        Random rand = new Random();
        int num=rand.nextInt(100)+1;
        huifutianxiaEnchashment.setReqSeqId(new SimpleDateFormat("yyyyMMdd").format(new Date()).toString());
        params.put("req_date", huifutianxiaEnchashment.getReqSeqId());
        params.put("req_seq_id", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
        params.put("cash_amt", huifutianxiaEnchashment.getCashAmt());
        params.put("huifu_id", huifutianxiaEnchashment.getHuifuId());//6666000141036927
        params.put("into_acct_date_type", "D0");
        params.put("token_no", huifutianxiaEnchashment.getTokenNo());//取现卡序列号  10035129284
        String reqData = JSONObject.toJSONString(params);
        String sortedData = JsonUtils.sort4JsonString(reqData, 0);
        String requestSign = RsaUtils.sign(sortedData,privateKey);
        Map<String, Object> request = new HashMap<>();
        request.put("sys_id", "6666000140068810");
        huifutianxiaEnchashment.setSysId("6666000140068810");
        request.put("data", JSONObject.parseObject(reqData));
        request.put("sign", requestSign);
        request.put("product_id", "XLSISV");
        String requestBody = JSON.toJSONString(request);
        log.info("汇付提现请求数据"+requestBody);
        String back = HttpClientUtils.httpPostJson("https://api.huifu.com/v2/trade/settlement/enchashment".toString(), headers, requestBody);
        log.info("汇付提现返回数据"+back);
        Map<String, Object> httpResponseMap = (Map<String, Object>) JSONObject.parse(back);
        Map<String, Object> responseDataMap = (Map<String, Object>) JSONObject.parse(httpResponseMap.get("data").toString());
        if(responseDataMap.get("resp_code").toString().equals("00000000")){
            huifutianxiaEnchashmentService.insertHuifutianxiaEnchashment(huifutianxiaEnchashment);
            return AjaxResult.success("处理成功");
        }else{
            return AjaxResult.error(responseDataMap.get("resp_desc").toString());
        }
    }

//    /**
//     * 提现
//     */
//    @GetMapping("/enchashment")
//    public String enchashment() throws Exception {
//        String privateKey ="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCRzZ0q2ycJkDPuJ24CthLQzfn2pjc2Q1yMAUFFkHwcc0zhZXlsGp+CQdR4cSrm55MlEJjtQzK4eTh/cDdEy3Ne3Ju9u14zTZqWzNMPOVa05tJ6xIjQj9dVxTBZ3jwP8qqDcdF8/TU1tfP2rCeFKcrKufPbyNZq9jUZj1r8v1mJlvLTXyzyCvzdET8szaeUcNh8QYV7VaGNGyUipzK0+8wRYqO9Y9xIte8I3zWrHVgz4nThguvBG+HY0hFyMs3u20OoG2dDNq/81rAQtSSdE6m+BSZgqLY7f8VGAA5Q74zLH4GU3sNOW3TJyxXgKRa0K8Mzw3THsMuEdgxeIK5dG1jjAgMBAAECggEAfDEmsY1wh/3TrgTZ+Oc7ya3ZP9W+67KfDwY2odl7TgSMNOVpcsKOobYPE/RZNcuFE6o6iMaXWniuviZIfYnKoL/KzObfS1XL2q1nDe2qRHbkS/xCCVysb/uBvtrEUFVEFqiJZyyK+VAeGTv0gcHiYaHY7Jn6wasr+bSZLPaUftPSlJlSKnzDIF+faE+IkQVceArm/sGtfPCYNcJOZksn/kd3H8FsqufJZePNF6F8SSB127l1j8zsXUWi7glYw2n7xiHzxVjBqYLut4dVOIv3dUBIacka3wYs0ug/bW9rvmSd5lOJmKKGAhRfHRnD2zmFKy/YkqSFnWcZPp724xStCQKBgQDFu3NADnqEaeC0I8c5Lq//c+8KzOkbnQz6A+LrvGiR9zLXHE3F544Mtjr1CbUbobLWPMBgtVDshkJ5t+VWB3r/nxiCjSYj3b02qVzmZq5eicHC0Z5PaLdVpNTLff9ukAndNGO8z+utB+tePOwi8jlYkXswVkZevmyaMS2hwpGr9QKBgQC8xLu/KLKM3tYN/usDN6INElvr5UXnbVHIWNFKlidwJ5IoB7+zUBlCF5Z3nkpqK6/su1pzVxnB6Kh+SodUc32sp0EiTai6AcrwcpNNO2AIn+2bLTA7hcHwQVKnD1lZtzXHZluTUVCweyJgn0bC/IHohNOr3fuBBVHCKh+fQIqCdwKBgEzJEGZtuPI204Xg5vqzwLDo6ok9LcEFagak/7gfwFP+tQWH/kO+OhGBqr+Bd401a+d6TBLCFpzjPmlaGnsgCIm/1JrOCWOgNlxFxMfMVCZuRIpNMLcRqTBfBCvJ0Cm1Ub9PGvQ1ogXdr283JVQk0FQumrIYrtcYB8CRHHFWx48JAoGBAKDKs2HROMXlyk+BpI3JBbA8wy/cy/6lj9QoU55E6LMQAd83OoNy5xD1JqYdnyVwsRnlPNaiir4xf9STh1LYTWST01d07xQCEutdrTtMKDEwiSR1CXCqLtiHy++hk2poNNb0yWxjB2hMeqvzAEN28vEss2DiZSkxbsMFPopYX6NhAoGBAIH5oBKUFDE3YFlzrkrTcEVyHf/KeLdeor8oLkTCyzFak9yMVqX36UCns2ch9R5zToXajtOoXNJvUoaD+zdznnDgzfLY3BsXiJz+VzjyxqnoDn1F1tSdZV1+lG3eaKZy7ATQHuiF4CxTFGILlnaBX3D6/JokUvf3JTXoSXIJXGc7";
//        Map<String, String> headers = new HashMap<>(4);
//        headers.put("Content-type", "application/json");
//        headers.put("sdk_version", "javaSDK_" + "3.0.5");
//        Map<String, Object> params = new HashMap<>();
//        Random rand = new Random();
//        int num=rand.nextInt(100)+1;
//        params.put("req_date", new SimpleDateFormat("yyyyMMdd").format(new Date()).toString());
//        params.put("req_seq_id", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
//        params.put("cash_amt", "1.11");
//        params.put("huifu_id", "6666000141036927");
//        params.put("into_acct_date_type", "D0");
//        params.put("token_no", "10035129284");//取现卡序列号
//        String reqData = JSONObject.toJSONString(params);
//        String sortedData = JsonUtils.sort4JsonString(reqData, 0);
//        String requestSign = RsaUtils.sign(sortedData,privateKey);
//        Map<String, Object> request = new HashMap<>();
//        request.put("sys_id", "6666000140068810");
//        request.put("data", JSONObject.parseObject(reqData));
//        request.put("sign", requestSign);
//        request.put("product_id", "XLSISV");
//        String requestBody = JSON.toJSONString(request);
//        log.info("汇付提现请求数据"+requestBody);
//        String back = HttpClientUtils.httpPostJson("https://api.huifu.com/v2/trade/settlement/enchashment".toString(), headers, requestBody);
//        log.info("汇付提现返回数据"+back);
//
//        return prefix + "/order";
//    }
}
