package com.ruoyi.web.controller.pay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.common.utils.http.HttpClient;
import com.ruoyi.common.utils.http.Md5_Sign;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.PayOrder;
import com.ruoyi.system.service.IPayOrderService;
import com.ruoyi.web.controller.demo.domain.UserBankModel;
import com.ruoyi.web.controller.demo.domain.UserOperateModel;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 汇聚支付
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/pay/huiju")
public class PayHuijuController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(PayHuijuController.class);
    @Autowired
    private ServerConfig serverConfig;
    @Autowired
    private IPayOrderService payOrderService;

    private static final String FILE_DELIMETER = ",";
    private String prefix = "pay";

    /**
     * 代付汇聚
     */
    @GetMapping("/danbidaifu")
    public String danbidaifu(ModelMap mmap) throws Exception{
//        String key = "052afa1bde004423a6c8b6f72a5262b6";
//
//        Map<String, Object> map = new HashMap<>();
//        map.put("userNo", "888121000003800");// 商户编号
//        String reqSign = getRequestSignYue(map);
//        // 签名
//        String hmac = Md5_Sign.SignByMD5(reqSign, key);
//        map.put("hmac", hmac);// 签名数据
//
//        // Map转json字符串
//        String reqBodyJson = JSON.toJSONString(map);
//        String httpResponseJson = HttpClient.sendHttpPost("https://www.joinpay.com/payment/pay/accountBalanceQuery",reqBodyJson);
//
//        //doResponseInfo(httpResponseJson, key);
//        Map<String, Object> httpResponseMap = (Map<String, Object>) JSONObject.parse(httpResponseJson);
//        Map<String, Object> dataMap = (Map<String, Object>) httpResponseMap.get("data");
//        mmap.put("userName",dataMap.get("userName"));
//        mmap.put("useAbleSettAmount",dataMap.get("useAbleSettAmount"));
        return prefix + "/huijudanbidaifu";
    }

    /**
     * 批量代付
     */
    @GetMapping("/daifuList")
    public String daifuList()
    {

        return prefix + "/daifuList";
    }
    @PostMapping("/daifuUplode")
    @ResponseBody
    public AjaxResult daifuUplode(MultipartFile file, boolean updateSupport) throws Exception {
        String key = "052afa1bde004423a6c8b6f72a5262b6";
        Map<String, Object> map = new HashMap<>();
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        List<PayOrder> payOrderList = new ArrayList<PayOrder>();

        Random rand = new Random();
        ExcelUtil<UserBankModel> util = new ExcelUtil<UserBankModel>(UserBankModel.class);
        List<UserBankModel> userList = util.importExcel(file.getInputStream());
        BigDecimal sumAmt = new BigDecimal("0");
        for(UserBankModel userBankModel : userList){
            PayOrder payOrder = new PayOrder();
            log.info(userBankModel.getBankName()+","+userBankModel.getBankNo()+","+userBankModel.getAmount());
            int num=rand.nextInt(100)+1;
            payOrder.setMerchantOrderId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
            Map<String, Object> map1 = new HashMap<>();
            map1.put("userNo","888121000003800"); //商户号
            map1.put("merchantOrderNo",payOrder.getMerchantOrderId()); //商户订单号
            map1.put("receiverAccountNoEnc",userBankModel.getBankNo()); //收款账户号
            map1.put("receiverNameEnc",userBankModel.getBankName()); //收款人
            map1.put("receiverAccountType",Integer.valueOf(201)); //账户类型
            //map1.put("receiverBankChannelNo",""); //收款账户联行号
            map1.put("paidAmount",new BigDecimal(userBankModel.getAmount())); //交易金额
            map1.put("currency","201"); //币种
            map1.put("isChecked","202"); //是否复核
            map1.put("paidDesc","工资发放"); //代付说明
            map1.put("paidUse","201"); //代付用途
            listMap.add(map1);
            sumAmt = sumAmt.add(new BigDecimal(userBankModel.getAmount()));

            payOrder.setChannelId(7L);
            payOrder.setChannelCode("888121000003800");
            payOrder.setAppId(2L);
            payOrder.setMerchantId(7L);
            payOrder.setChannelOrderNo("0");
            payOrder.setSubject("工资发放");
            payOrder.setBody(userBankModel.getBankName()+","+userBankModel.getBankNo()+","+userBankModel.getAmount());
            payOrder.setNotifyUrl("无");
            payOrder.setNotifyStatus(0L);
            payOrder.setAmount(Long.valueOf(userBankModel.getAmount()));
            payOrder.setStatus(0L);
            payOrder.setUserIp("0.0.0.0");
            payOrder.setCreateTime(DateUtils.getNowDate());
            payOrderList.add(payOrder);
        };

        map.put("userNo","888121000003800");//商户编号
        map.put("productCode","BANK_PAY_DAILY_ORDER");//产品类型
        int num=rand.nextInt(100)+1;
        map.put("merchantBatchNo","PL"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num); // 商户批次号
        map.put("requestCount",Integer.valueOf(userList.size()));// 请求总笔数
        map.put("requestAmount",sumAmt); //请求总金额
        map.put("requestTime",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()).toString()); // 交易请求时间
        map.put("callbackUrl","");//商户通知地址
        map.put("firstProductCode","");//优先使用产品
        map.put("details",listMap); //代付明细数据

        String reqSign = getRequestSign(map);
        // 签名
        String hmac = Md5_Sign.SignByMD5(reqSign, key);
        map.put("hmac", hmac);/** 签名数据 */

        // Map转json字符串
        String reqBodyJson = JSON.toJSONString(map);
        log.info("reqBodyJson:"+reqBodyJson);
        String httpResponseJson = HttpClient.sendHttpPost("https://www.joinpay.com/payment/pay/batchPay",reqBodyJson);
        log.info("httpResponseJson:"+httpResponseJson);

        Map<String, Object> httpResponseMap = (Map<String, Object>) JSONObject.parse(httpResponseJson);
        if(httpResponseMap.get("statusCode").toString().equals("2001")){
            for(PayOrder payOrder12:payOrderList){
                payOrderService.insertPayOrder(payOrder12);
            }
            return AjaxResult.success("处理成功");
        }else{
            return AjaxResult.error(httpResponseMap.get("message").toString());
        }

    }


    public static String getRequestSignYue(Map<String, Object> params) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(params.get("userNo"));


        return stringBuilder.toString();
    }

    /**
     * 获取请求数据签名串信息
     * 必须按新代付接口文档请求参数信息顺序来进行字符串的拼接，详情请参考新代付接口文档请求报文
     *
     * @param params 请求数据参数
     * @return 返回请求签名串
     */
    public static String getRequestSign(Map<String, Object> params) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(params.get("userNo")).append(params.get("productCode"))
                .append(params.get("merchantBatchNo")).append(params.get("requestCount"))
                .append(params.get("requestAmount")).append(params.get("requestTime"))
                .append(params.get("callbackUrl")).append(params.get("firstProductCode"))
                .append(details(params));
        return stringBuilder.toString();
    }

    public static String details(Map<String, Object> params) {
        StringBuilder stringBuilder = new StringBuilder();
        Object object = params.get("details");
        System.err.println(object);
        ArrayList<Map<String, Object>> details = (ArrayList<Map<String, Object>>) params.get("details");
        System.out.println("details"+details);
        for(Map<String, Object> detail : details) {
            stringBuilder.append(detail.get("userNo")).append(detail.get("merchantOrderNo"))
                    .append(detail.get("receiverAccountNoEnc")).append(detail.get("receiverNameEnc"))
                    .append(detail.get("receiverAccountType"));
            if(detail.get("receiverBankChannelNo") !=null) {
                stringBuilder.append(detail.get("receiverBankChannelNo"));
            }
            stringBuilder.append(detail.get("paidAmount"));
            stringBuilder.append(detail.get("currency")).append(detail.get("isChecked"));
            stringBuilder .append(detail.get("paidDesc")).append(detail.get("paidUse"));
        }
        return stringBuilder.toString();
    }

}
