package com.ruoyi.web.controller.tool;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.http.CertUtil;
import com.ruoyi.common.utils.http.CryptoUtil;
import com.ruoyi.common.utils.http.HttpClient;
import com.ruoyi.common.utils.http.SDKUtil;
import com.ruoyi.common.utils.huifutianxia.HttpClientUtils;
import com.ruoyi.common.utils.huifutianxia.JsonUtils;
import com.ruoyi.common.utils.huifutianxia.RsaUtils;
import com.ruoyi.system.domain.PayMerchant;
import com.ruoyi.system.domain.PayOrder;
import com.ruoyi.system.service.IPayMerchantService;
import com.ruoyi.system.service.IPayOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * swagger 支付接口
 * 
 * @author ruoyi
 */
@Api("汇付天下支付接口")
@RestController
@RequestMapping("/api/pay/huifutianxia")
public class PayHuifutianxiaApiController extends BaseController
{
    private static Logger log = LoggerFactory.getLogger(PayHuifutianxiaApiController.class);
    @Autowired
    private IPayOrderService payOrderService;
    @Autowired
    private IPayMerchantService payMerchantService;

    @ApiOperation("统一支付接口")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "merchantId", value = "商家商户ID", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "payTool", value = "支付工具: 微信扫码：0402", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "orderCode", value = "长度12位起步，商户唯一，建议订单号有日期", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "totalAmount", value = "单位元，需保留小数点后两位，示例值：1000.00，最低传入0.01", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "subject", value = "商品描述", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "notifyUrl", value = "回调地址", dataType = "String", dataTypeClass = String.class),
    })
    @PostMapping("/jspay")
    public R<String> jspay(String merchantId, String payTool, String orderCode, String totalAmount, String subject, String notifyUrl) throws Exception
    {
        String privateKey ="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDf7XgqR1nj6PPYd0I0bRBaT44q/4L/rp/55xFbHTQU80vczloC1mtEzLBSVLFq2o+veuN/xlZm/x722szCK/s/ONt2dkMzkzk96r2YSaOrJA9yECo2NWqzTzA7qK29t5hbFSc88Jt36mINnEV07aGTsA/Wwv9DIOxPAbvp7DDNoV0BpeFfCbURLknDzPWT0zZi4YE/UZaeeYTPjt2vHQTtH8W8WwcZ2briSceWK4cBqS3l+o1GTDpJLfei7RITkOh/o0bgxkwbJlIaAGMxKa0T3XUWl05YS221YwD/wWkhvsTNlzKhdAQM5GVm/X0Q6k4zb1nnkzz1V5PUJg3IWbXPAgMBAAECggEAQbHujiDOu3GNkJU0ZCMXKBes8upxaUe3AV8MFXoU3IygVhBoEesMH2wZ0p2sS1gBzY61DbiHgsIYOH3qkpDKrjqlEDUT9B6eLY8fvnzkOoJi0ajyaWL0gdSapvYOutkRJq+nClbglaa6y5lsOyi4PZbqnqworW4MRgWRNsinbLv8zTJk5PA7h77kcdyYVf4ePjzMZMkZkokAYxogBZ2J33Ap/YOWQ+9BEanoeinfPTff7C0pCPJNCdfEF4BFDiLk140SpqPAG7Ob7pjxOBdamUciuAySCET0GhlAX6dCXFsHGivHSRx8nwkgqcK5acMVKErh5Y7ksg+tXgOSkfKISQKBgQD99uwMgtAXusgx7LAJZazkZgur/svz64dNjn6YzOhP/AS1F+WnV2w70s8vCt28JVIQAIAQ0AskXa+knIDjEoUJxSZK/6vwEUTBmPXv4hymijtEzrmq4uLbOzR3KAWmTDF6j5ndeAxJnIaq18+Zj4r1fNOoFCuVpQV3qXJJ+JRmGwKBgQDhuOsYJdporbJzKyCstwthtRvi1qamqIbIAlu1P8Kbf3cMyRRM2IXm5vZ8h33CdT5nKoATVA7PUuyHu+wrZ1tD7UjawaKcfRU3etZ/z45NHCarQ+u5aqMcj/kdK43FsEre4kDl8j7sLOQakortyoQtAoA8xW1m8C8uGw7vgNO6XQKBgQDSya5UGAMVnv7oiRX3Rr/Adn47jGjFlHihw6KJupEbSgayNBp10VbRNICn//HYZnV9OJ0S10pYLbZJ/2fukIGBgXPuDdUslXim4wlFyDuVcugMjTSHhHdcFQDP4ZUG2Pi8TBIeQrSYKGWRyTvSrdRdOvUF7W/bbbNjBW4JvVuWfwKBgE1ftu67IwgY6n/B+9xYjoU6sMlsLQoZ4MG01TznE018PxHZ56SD4DqLV1jOlRRQXki879QwTzdQRJYum8ExF77MGtop4BTmZ5xx9g8v1aanDLZOfQI9BJao34rZQCrQyIloYzivdlQ8+yAEtazXa8GGBvFN97d+g525VsBQl8NdAoGBAKCOWAElsaE+2Pn3L8GHDkKAY/fsFZnDHmawBW31vAaOanh05zpXA752CGj/5dH9LYCroR2pZK/G7zPlOtnVo52aYuIPOy5Pinr8U7c3aXFeGbWei/wCOwR5CjOzbCuSpxJlyB2mm5z/ICgUX3Wsv236qu86ZfUebfVX+fr4aWR7";
        Map<String, String> headers = new HashMap<>(4);
        headers.put("Content-type", "application/json");
        headers.put("sdk_version", "javaSDK_" + "3.0.5");

        Map<String, Object> params = new HashMap<>();
        params.put("huifu_id", "6666000140917403");
        Random rand = new Random();
        int num=rand.nextInt(100)+1;
        params.put("req_seq_id", "rQ"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
        params.put("req_date", new SimpleDateFormat("yyyyMMdd").format(new Date()).toString());
        params.put("goods_desc",subject);
        params.put("trade_type",payTool);
        params.put("trans_amt",totalAmount);
        String reqData = JSONObject.toJSONString(params);
        String sortedData = JsonUtils.sort4JsonString(reqData, 0);
        String requestSign = RsaUtils.sign(sortedData,privateKey);

        Map<String, Object> request = new HashMap<>();
        request.put("sys_id", "6666000140917403");
        request.put("data", JSONObject.parseObject(reqData));
        request.put("sign", requestSign);
        request.put("product_id", "PAYUN");
        String requestBody = JSON.toJSONString(request);
        log.info("汇付天下扫码支付请求数据"+requestBody);
        String back = HttpClientUtils.httpPostJson("https://api.huifu.com/v2/trade/payment/jspay".toString(), headers, requestBody);
        log.info("汇付天下扫码支付返回数据"+back);
        return R.ok(back);

    }



}
