package com.ruoyi.web.controller.pay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.http.HttpClient;
import com.ruoyi.common.utils.http.Md5_Sign;
import com.ruoyi.system.domain.PayOrder;
import com.ruoyi.system.service.IPayOrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * 汇付宝
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/pay/huifubao")
public class PayHuifubaoController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(PayHuifubaoController.class);

    private String prefix = "pay";
    //易收款，网银支付，快捷支付，批付+分账   pay200@aol.com qqwwee112233  qqwweerr11223344   主:2161799  子：2170061
    //签名密钥：E66C20BF83B140EABC96B443       90A9B7CDD5694F0B94739C3E
    //3DES加密密钥：0C12FC8CF67D49A9A0B81B49   F34842FAD87E444FA56D28FD
    //义乌国骅科技发展有限公司
    //汇付宝
    //账号:19170310333@163.com
    //密码:Aa666999
    //支付密码:aa369369

    /**
     * 用户余额查询跳转
     */
    @GetMapping("/tradesubmit")
    public String userQueryList()
    {
        // 目标URL
        String url = "https://Pay.Heepay.com/API/Allot/tradesubmit.aspx";
        //分润需要的MD5密钥，商户后台网关密钥下面第一个
        String key = "0C12FC8CF67D49A9A0B81B49";
        String version = "3";               //当前接口版本号
        String agent_id = "1664502";    //商户ID

        return prefix + "/userQuery";
    }


}
