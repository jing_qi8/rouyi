package com.ruoyi.web.controller.pay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.common.utils.http.HttpClient;
import com.ruoyi.common.utils.http.Md5_Sign;
import com.ruoyi.framework.web.service.ConfigService;
import com.ruoyi.system.domain.PayOrder;
import com.ruoyi.system.service.IPayOrderService;
import com.ruoyi.web.controller.common.CommonController;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 支付控制台模块
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/pay")
public class PayController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(PayController.class);
    @Autowired
    private ServerConfig serverConfig;
    @Autowired
    private IPayOrderService payOrderService;

    private static final String FILE_DELIMETER = ",";
    private String prefix = "pay";

    /**
     * 代付支付订单查询
     */
    @GetMapping("/daifuOrderList")
    public String daifuOrderList()
    {
        return prefix + "/daifuorder";
    }
    /**
     * 支付订单查询
     */
    @GetMapping("/orderList")
    public String orderList()
    {

        return prefix + "/order";
    }

    /**
     * 代付汇聚
     */
    @RequiresPermissions("pay:singlePay")
    @PostMapping("/singlePay")
    @ResponseBody
    public AjaxResult singlePay(String receiverAccountNoEnc,String receiverNameEnc,String paidAmount) throws Exception{
        PayOrder payOrder = new PayOrder();
        Random rand = new Random();
        int num=rand.nextInt(100)+1;
        payOrder.setMerchantOrderId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
        String key = "052afa1bde004423a6c8b6f72a5262b6";
        Map<String, Object> map = new HashMap<>();
        map.put("userNo","888121000003800");// 商户编号
        map.put("productCode", "BANK_PAY_DAILY_ORDER");//产品类型
        map.put("requestTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()).toString()); // 交易请求时间
        map.put("merchantOrderNo", payOrder.getMerchantOrderId());//商户订单号
        map.put("receiverAccountNoEnc", receiverAccountNoEnc); // 收款账户号
        map.put("receiverNameEnc", receiverNameEnc); // 收款人
        map.put("receiverAccountType", Integer.valueOf(201));//账户类型 对私账户：201    对公账户：204
        map.put("receiverBankChannelNo", ""); // 收款账户联行号
        map.put("paidAmount", new BigDecimal(paidAmount) ); //交易金额
        map.put("currency", "201");//币种
        map.put("isChecked", "202");//是否复核
        map.put("paidDesc", "工资发放");//代付说明
        map.put("paidUse", "201");//代付用途
        map.put("callbackUrl", "");//商户通知地址
        map.put("firstProductCode", "");//优先使用产品

        String reqSign = getRequestSign(map);
        // 签名
        String hmac = Md5_Sign.SignByMD5(reqSign, key);
        map.put("hmac", hmac);/** 签名数据 */

        // Map转json字符串
        String reqBodyJson = JSON.toJSONString(map);
        log.info("reqBodyJson：" + reqBodyJson);
        String httpResponseJson = HttpClient.sendHttpPost("https://www.joinpay.com/payment/pay/singlePay",reqBodyJson);
        doResponseInfo(httpResponseJson, key);
        payOrder.setChannelId(7L);
        payOrder.setChannelCode("888121000003800");
        payOrder.setAppId(2L);
        payOrder.setMerchantId(7L);
        payOrder.setChannelOrderNo("0");
        payOrder.setSubject("工资发放");
        payOrder.setBody(receiverNameEnc+","+receiverAccountNoEnc+","+paidAmount);
        payOrder.setNotifyUrl("无");
        payOrder.setNotifyStatus(0L);
        payOrder.setAmount(Long.valueOf(paidAmount));
        payOrder.setStatus(0L);
        payOrder.setUserIp("0.0.0.0");
        payOrder.setCreateTime(DateUtils.getNowDate());

        return toAjax(payOrderService.insertPayOrder(payOrder));
    }

    /**
     * 对单笔代付响应信息的处理
     *
     * @param httpResponseJson 响应信息json字符串
     * @param key 商户秘钥
     * @throws Exception 异常实体类
     */
    @SuppressWarnings("unchecked")
    private static void doResponseInfo(String httpResponseJson, String key) throws Exception {

        // 响应信息map集合
        Map<String, Object> httpResponseMap = (Map<String, Object>) JSONObject.parse(httpResponseJson);
        // 业务数据map集合
        Map<String, Object> dataMap = (Map<String, Object>) httpResponseMap.get("data");
        dataMap.put("statusCode", httpResponseMap.get("statusCode"));
        dataMap.put("message", httpResponseMap.get("message"));

        // 请求签名串
        String reqSign = getRequestSign(httpResponseMap);
        // 响应签名串
        String respSign = getResponseSign(dataMap);
        // 请求数据的加密签名
        String reqHmac = Md5_Sign.SignByMD5(respSign, key);
        // 请求数据的加密签名
        String respHmac = (String) dataMap.get("hmac");
        log.info("reqHmac：" + reqHmac);
        log.info("respSign：" + respHmac);

        reqHmac=reqHmac.toUpperCase();
        respHmac=respHmac.toUpperCase();
        boolean isMatch = reqHmac.equals(respHmac);
        if (isMatch) {
            log.info("验签成功");
        } else {
            log.info("验签失败");
        }
    }

    /**
     * 获取请求数据签名串信息
     * 必须按新代付接口文档请求参数信息顺序来进行字符串的拼接，详情请参考新代付接口文档请求报文
     *
     * @param params 请求数据参数
     * @return 返回请求签名串
     */
    public static String getRequestSign(Map<String, Object> params) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(params.get("userNo")).append(params.get("productCode")).append(params.get("requestTime"))
                .append(params.get("merchantOrderNo")).append(params.get("receiverAccountNoEnc"))
                .append(params.get("receiverNameEnc")).append(params.get("receiverAccountType"))
                .append(params.get("receiverBankChannelNo")).append(params.get("paidAmount"))
                .append(params.get("currency")).append(params.get("isChecked")).append(params.get("paidDesc"))
                .append(params.get("paidUse")).append(params.get("callbackUrl")).append(params.get("firstProductCode"));
        log.info("reqSign：" + stringBuilder.toString());
        return stringBuilder.toString();
    }

    /**
     * 获取响应数据签名串信息
     * 必须按新代付接口文档应答参数信息顺序来进行字符串的拼接，详情请参考新代付接口文档的应答报文
     *
     * @param params 响应数据参数
     * @return 返回响应签名串
     */
    public static String getResponseSign(Map<String, Object> params) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(params.get("statusCode")).append(params.get("message")).append(params.get("errorCode"))
                .append(params.get("errorDesc")).append(params.get("userNo")).append(params.get("merchantOrderNo"));

        return stringBuilder.toString();
    }

}
