package com.ruoyi.web.controller.demo.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 用户银行卡信息
 * 
 * @author ruoyi
 */
public class UserBankModel
{
    /**
     * 商品名称
     */
    @Excel(name = "收款银行户")
    private String bankName;

    /**
     * 商品重量
     */
    @Excel(name = "收款银行账号")
    private String bankNo;

    /**
     * 商品价格
     */
    @Excel(name = "金额（元）")
    private String amount;

    @Override
    public String toString() {
        return "UserBankModel{" +
                "bankName='" + bankName + '\'' +
                ", bankNo='" + bankNo + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
