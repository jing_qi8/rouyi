package com.ruoyi.web.controller.tool;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.http.CertUtil;
import com.ruoyi.common.utils.http.CryptoUtil;
import com.ruoyi.common.utils.http.HttpClient;
import com.ruoyi.common.utils.http.SDKUtil;
import com.ruoyi.system.domain.PayMerchant;
import com.ruoyi.system.domain.PayOrder;
import com.ruoyi.system.service.IPayMerchantService;
import com.ruoyi.system.service.IPayOrderService;
import io.swagger.annotations.*;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * swagger 支付接口
 * 
 * @author ruoyi
 */
@Api("支付接口")
@RestController
@RequestMapping("/api/pay")
public class PayApiController extends BaseController
{
    private static Logger logger = LoggerFactory.getLogger(PayApiController.class);
    @Autowired
    private IPayOrderService payOrderService;
    @Autowired
    private IPayMerchantService payMerchantService;
    private static int connectTimeout = 3000;
    private static int readTimeout = 15000;

    JSONObject header = new JSONObject();
    JSONObject body=new JSONObject();

    @ApiOperation("统一支付接口")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "merchantId", value = "商家商户ID", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "payTool", value = "支付工具: 微信扫码：0402", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "orderCode", value = "长度12位起步，商户唯一，建议订单号有日期", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "totalAmount", value = "例 000000000101 代表 1.01 元", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "subject", value = "订单标题", dataType = "String", dataTypeClass = String.class),
        @ApiImplicitParam(name = "notifyUrl", value = "回调地址", dataType = "String", dataTypeClass = String.class),
    })
    @PostMapping("/create")
    public R<String> save(String merchantId, String payTool, String orderCode, String totalAmount, String subject, String notifyUrl) throws Exception
    {

        PayMerchant payMerchant = payMerchantService.selectPayMerchantById(Long.valueOf(merchantId));
        if(payMerchant == null){
            return R.fail("商家商户ID参数错误");
        }
        PayOrder payOrder = new PayOrder();
        payOrder.setChannelId(payMerchant.getId());
        payOrder.setChannelCode(payMerchant.getNo());
        payOrder.setAppId(01L);
        payOrder.setMerchantId(payMerchant.getId());
        payOrder.setChannelOrderNo(orderCode);
        Random rand = new Random();
        int num=rand.nextInt(100)+1;
        payOrder.setMerchantOrderId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);
        payOrder.setSubject(subject);
        payOrder.setBody("无");
        payOrder.setNotifyUrl(notifyUrl);
        payOrder.setNotifyStatus(0L);
        payOrder.setAmount(Long.valueOf(totalAmount));
        payOrder.setStatus(0L);
        payOrder.setUserIp("0.0.0.0");
        payOrder.setCreateTime(DateUtils.getNowDate());

        String reqAddr="/qr/api/order/create";   //接口报文规范中获取

        //加载证书
        CertUtil.init(payMerchant.getPublicKeyPath(), payMerchant.getPrivateKeyPath(), payMerchant.getKeyPassword());
        //设置报文头
        this.setHeader();
        //设置报文体
        body.put("payTool", payTool);//支付工具: 0401-支付宝扫码
        body.put("orderCode", payOrder.getMerchantOrderId());		//商户订单号
//		body.put("limitPay","5");							//限定支付方式 送1-限定不能使用贷记卡	送4-限定不能使用花呗	送5-限定不能使用贷记卡+花呗
        body.put("totalAmount",totalAmount );			//订单金额 12位长度，精确到分
        body.put("subject", subject);						//订单标题
        body.put("body", "");				//订单描述
//        body.put("txnTimeOut",DemoBase.getNextDayTime());	//订单超时时间
//		body.put("payAccLimit","WXPAY");	//限制扫码支付方式 ALIPAY-支付宝 CUPPAY-银联
        body.put("storeId", "");							//商户门店编号
        body.put("terminalId", "");							//商户终端编号
        body.put("operatorId", "");							//操作员编号
        body.put("notifyUrl", notifyUrl);	//异步通知地址
        body.put("bizExtendParams", "");					//业务扩展参数
        body.put("merchExtendParams", "");					//商户扩展参数
        body.put("hbFqFlag", "");							//花呗分期标识
        body.put("hbFqNum", "");							//花呗分期期数
        body.put("hbFqSellerPercent", "");					//卖家承担手续费比例
        body.put("extend", "");								//扩展域

        JSONObject resp=this.requestServer(header, body, reqAddr,payMerchant.getPayUrl());

        if(resp.getJSONObject("head").getString("respCode").equals("000000")) {
            logger.info("生产二维码成功");
            logger.info("生成的二维码信息为："+resp.getJSONObject("body").getString("qrCode"));
            payOrder.setBody(resp.getJSONObject("body").getString("qrCode"));
            payOrderService.insertPayOrder(payOrder);
            return R.ok(resp.getJSONObject("body").getString("qrCode"),"成功");
        }else {
            return R.fail(resp.getJSONObject("head").getString("respMsg"));
        }

    }

    public static JSONObject requestServer(JSONObject header,JSONObject body,String reqAddr,String payUrl) {

        Map<String, String> reqMap = new HashMap<String, String>();
        JSONObject reqJson = new JSONObject();
        reqJson.put("head", header);
        reqJson.put("body", body);
        String reqStr = reqJson.toJSONString();
        String reqSign;
        // 签名
        try {
            reqSign = new String(Base64.encodeBase64(CryptoUtil.digitalSign(reqStr.getBytes("UTF-8"), CertUtil.getPrivateKey(), "SHA1WithRSA")));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
        //整体报文格式
        reqMap.put("charset", "UTF-8");
        reqMap.put("data", reqStr);
        reqMap.put("signType", "01");
        reqMap.put("sign", reqSign);
        reqMap.put("extend", "");

        String result;
        try {
            logger.info("请求报文：\n" + JSONObject.toJSONString(reqJson, true));
            result = HttpClient.doPost(payUrl + reqAddr, reqMap, connectTimeout, readTimeout);
            result = URLDecoder.decode(result, "UTF-8");
        } catch (IOException e) {
            logger.error(e.getMessage());
            return null;
        }


        Map<String, String> respMap = SDKUtil.convertResultStringToMap(result);
        String respData = respMap.get("data");
        String respSign = respMap.get("sign");

        // 验证签名
        boolean valid;
        try {
            valid = CryptoUtil.verifyDigitalSign(respData.getBytes("UTF-8"), Base64.decodeBase64(respSign), CertUtil.getPublicKey(), "SHA1WithRSA");
            if (!valid) {
                logger.error("verify sign fail.");
                return null;
            }
            logger.info("verify sign success");
            JSONObject respJson = JSONObject.parseObject(respData);
            if (respJson != null) {
                logger.info("响应码：[" + respJson.getJSONObject("head").getString("respCode") + "]");
                logger.info("响应描述：[" + respJson.getJSONObject("head").getString("respMsg") + "]");
                logger.info("响应报文：\n" + JSONObject.toJSONString(respJson, true));
            } else {
                logger.error("服务器请求异常！！！");
            }
            return respJson;

        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    public void setHeader() {

        header.put("version", "1.0");			//版本号
        header.put("method", "sandpay.trade.precreate");		//接口名称:预下单
        header.put("productId", "00000005");//产品编码
        header.put("mid", "6888800121502");	//商户号
        header.put("accessType", "1");					//接入类型设置为普通商户接入
        header.put("channelType", "07");					//渠道类型：07-互联网   08-移动端
        header.put("reqTime", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));	//请求时间
    }


//    public void setBody() {
//        body.put("payTool", "0402");//支付工具: 0401-支付宝扫码
//        Random rand = new Random();
//        int num=rand.nextInt(100)+1;
//        body.put("orderCode", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+num);		//商户订单号
////		body.put("limitPay","5");							//限定支付方式 送1-限定不能使用贷记卡	送4-限定不能使用花呗	送5-限定不能使用贷记卡+花呗
//        body.put("totalAmount","000000000003" );			//订单金额 12位长度，精确到分
//        body.put("subject", "话费充值");						//订单标题
//        body.put("body", "用户购买话费0.03");				//订单描述
////        body.put("txnTimeOut",DemoBase.getNextDayTime());	//订单超时时间
////		body.put("payAccLimit","WXPAY");	//限制扫码支付方式 ALIPAY-支付宝 CUPPAY-银联
//        body.put("storeId", "");							//商户门店编号
//        body.put("terminalId", "");							//商户终端编号
//        body.put("operatorId", "");							//操作员编号
//        body.put("notifyUrl", "http://xxx.com/notify");	//异步通知地址
//        body.put("bizExtendParams", "");					//业务扩展参数
//        body.put("merchExtendParams", "");					//商户扩展参数
//        body.put("hbFqFlag", "");							//花呗分期标识
//        body.put("hbFqNum", "");							//花呗分期期数
//        body.put("hbFqSellerPercent", "");					//卖家承担手续费比例
//        body.put("extend", "");								//扩展域
////		body.put("accsplitInfo", "{\"accsplitMode\":\"3\"}");	//分账域 不分账请注释掉此行
//    }


}
